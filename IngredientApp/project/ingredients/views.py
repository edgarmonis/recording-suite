import datetime, json, copy, csv, os
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render


def render_home(request):
    return render(request, "index.html")
