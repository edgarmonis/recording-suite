#!/bin/bash
# sleep $DB_INIT_SLEEP
cd /app/ingredient/project
python manage.py makemigrations
python manage.py migrate

# ssh -o StrictHostKeyChecking=no pi@192.168.0.55
# ssh -o StrictHostKeyChecking=no pi@192.168.0.79
# ssh -o StrictHostKeyChecking=no pi@192.168.0.70
# ssh -o StrictHostKeyChecking=no pi@192.168.0.82
# ssh -o StrictHostKeyChecking=no pi@192.168.0.80

# python manage.py runserver
gunicorn --bind 0.0.0.0:8000 wsgi:application --timeout 100
# gunicorn --bind=0.0.0.0:8000 --workers $NUM_WORKERS --max-requests=1000 -c /app/ingredient/project/docker_gunicorn_configuration.py project.wsgi:application