const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "./django_output"),
    filename: "[name].js",
  },
  module: {
    rules: [
        {
            test: /\.js|.jsx$/,
            exclude: /node_modules/,
            use: "babel-loader",
          },
          {
            test: /\.css$/,
            use: [
                "style-loader",
                "css-loader",
                {
                    loader: "postcss-loader",
                    options: {
                        postcssOptions : {
                            plugins: () => [
                                require("autoprefixer")()
                            ],
                        }
                    },
                },
            ],
          },
          {
            test: /\.s[ac]ss$/i,
            use: [
              // Creates `style` nodes from JS strings
              "style-loader",
              // Translates CSS into CommonJS
              "css-loader",
              // Compiles Sass to CSS
              "sass-loader",
            ],
          },
    ],
  },
  // optimization: {
  //   minimize: true,
  // },
  // plugins: [
  //   new webpack.DefinePlugin({
  //     "process.env": {
  //       NODE_ENV: JSON.stringify("development"),
  //     },
  //   }),
  // ],
};