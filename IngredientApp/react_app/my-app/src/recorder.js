import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import './index.scss';

import differenceBy from 'lodash/differenceBy';

import Select from 'react-select'
import makeAnimated from 'react-select/animated';
import DataTable from 'react-data-table-component';

import FloatingLabel from 'react-bootstrap/esm/FloatingLabel';
import Modal from 'react-bootstrap/Modal'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Navbar from 'react-bootstrap/Navbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Cookies from 'universal-cookie';
 
const cookies = new Cookies();
 


const animatedComponents = makeAnimated();

const groupStyles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
};
const groupBadgeStyles = {
  backgroundColor: '#EBECF0',
  borderRadius: '2em',
  color: '#172B4D',
  display: 'inline-block',
  fontSize: 12,
  fontWeight: 'normal',
  lineHeight: '1',
  minWidth: 1,
  padding: '0.16666666666667em 0.5em',
  textAlign: 'center',
};

const formatGroupLabel = data => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

class Webpage extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            brand_options : ['KFC', 'Break', 'Mcdonalds', 'Bruh'],
            recipe_options : ['butter chicken', 'tikka', 'veg', 'humans'],
            ingredient_options : ['carrot', 'potato', 'peas', 'cranium'],
            sub_recipe_options : ['garlic', 'ginger', 'break', 'lol'],
            qty_options : ['100 gms', '200 gms'],
            version_options: [],
            ingredient_and_weights : [ ['', 0] ],
            
            setData: false,
          
            brand_searchable_options : [],
            recipe_searchable_options : [],
            sub_recipe_searchable_options : [] ,
            ingredient_searchable_options : [],
            quantity_searchable_options : [],
            version_searchable_options : [],
          
            cur_table_id : 1,

            ingredient_modal_open : false,
            ingredient_modal_sub_recipe : "",
            ingredient_modal_quantity : "",

            recipe_modal_open : false,
            brand_selection: "Brand",
            recipe_selection: "Recipe",
            sub_recipe_selection: "Sub Recipe",
            quantity_selection: "Quantity",
            version_selection: "Version",
            output_weight: 0,
            ingredient_selected_options: []
        }
    }

    renderIngredientModal(variable){
      return (
      <Modal show={variable} onHide={this.handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Component</Modal.Title>
        </Modal.Header>
        

        <FloatingLabel controlId="Ingredient" label="Ingredient">
          <Form.Control type="text" placeholder="Ingredient" onChange={e => this.setState({ ingredient_modal_sub_recipe: e.target.value })}/>
        </FloatingLabel>

        <FloatingLabel controlId="Quantity" label="Quantity">
          <Form.Control type="number" placeholder="Quantity" onChange={e => 
            this.setState({ 
              ingredient_modal_quantity: e.target.value,
              sub_recipe_selection : e.target.value })
            }/>
        </FloatingLabel>

          <Button variant="secondary" onClick={this.handleModalClose} >
            Close
          </Button>
          <Button variant="primary" onClick={this.handleIngredientModalAdd} >
            Add
          </Button>
      </Modal>
      );
    }

    renderRecipeModal(variable){
      return (
      <Modal show={variable} onHide={this.handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Recipe To DB</Modal.Title>
        </Modal.Header>

        <Modal.Body className="show-grid">
          <Container>
            <Row>
              <Col xs={12} md={6}>
                {
                  this.renderSearch(this.state.sub_recipe_searchable_options, "Mod Existing Recipe", this.handleNewRecipeSelect)
                }
              </Col>
              <Col xs={12} md={6}>
                <FloatingLabel controlId="SubRecipe" label="SubRecipe">
                  <Form.Control type="text" value={this.state.recipe_modal_sub_recipe} onChange={e => this.setState({ recipe_modal_sub_recipe: e.target.value })}/>
                </FloatingLabel>
              </Col>
            </Row>

            <Row>
              <Col xs={12} md={6}>
                {
                  this.renderSearch(this.state.quantity_searchable_options, "Mod Existing Qty", this.handleNewQtySelect)
                }
              </Col>
              <Col xs={12} md={6}>
              <FloatingLabel controlId="Quantity" label="Quantity">
                <Form.Control type="number" value={this.state.recipe_modal_quantity} onChange={e => this.setState({ recipe_modal_quantity: e.target.value })}/>
              </FloatingLabel>
              </Col>
            </Row>
          </Container>
        </Modal.Body>



        <Button variant="secondary" onClick={this.handleModalClose} fluid>
          Close
        </Button>
        <Button variant="primary" onClick={this.handleRecipeModalAdd} fluid>
          Add
        </Button>
      </Modal>
      );
    }

    renderGreatTable(data, columns){
      return (
        <DataTable
          title="Ingredients"
          columns={columns}
          data={data}
          selectableRows
          contextComponent={
            <Button key="delete" onClick={this.handleDelete} style={{ backgroundColor: 'red' }}>
              Delete
            </Button>
          }
          onSelectedRowsChange={this.handleRowSelected}
        />
      );

    }

    renderButton(text, id, onClick){
        if(onClick!=='null'){
            return(
              <ButtonGroup className="d-flex">
                <Button variant="primary" id={id} onClick={onClick}>{text}</Button>
              </ButtonGroup>
                );
        }
        else{
            return(
                <ButtonGroup className="d-flex">
                  <Button variant="primary" id={id} >{text}</Button>
                </ButtonGroup>
                );
        }
    }

    renderIngredientSearch(options, placeholder, onChange){
      return(
          <Select 
            options={options}
            components={animatedComponents}
            isClearable={true}
            isMulti
            placeholder={placeholder}
            defaultValue={this.state.ingredient_selected_options}
            value={this.state.ingredient_selected_options}
            onChange={onChange}
            closeMenuOnSelect={true}
            options={options}
            formatGroupLabel={formatGroupLabel}
          />
      );
    }

    renderSearch(options, placeholder, onChange){
      return(
          <Select 
            options={options}
            isClearable={true}
            placeholder={placeholder}
            onChange={onChange}
          />
      );
    }

    handleBrandSelect = (e) => {
      if(e!=null){
        cookies.set('brand', e['value'], { path: '/' });
        this.getRecipes(e)
      }
    }

    handleRecipeSelect = (e) => {
      if(e!=null){
        cookies.set('recipe', e['value'], { path: '/' });
        this.getSubRecipes(e)
      }
    }

    handleNewRecipeSelect = (e) => {
      if(e!=null){
        console.log("handle new sub recipe called")
        this.setState({
          recipe_modal_sub_recipe : e['value']
        })
        this.getQuantity(e)
      }
    }

    handleNewQtySelect = (e) => {
      if(e!=null){
        console.log("handle new qty called")
        this.setState({
          recipe_modal_quantity : e['value']
        })
      }
    }

    handleSubRecipeSelect = (e) => {
      if(e!=null){
        cookies.set('sub_recipe', e['value'], { path: '/' });
        cookies.set('record_name', e['value'], { path: '/' });
        this.setState({
          sub_recipe_selection : e['value']
        })
        this.getQuantity(e)
      }
    }

    handleQuantitySelect = (e) => {
      if(e!=null){
        cookies.set('quantity', e['value'], { path: '/' });
        console.log('inside quantity select')
        this.setState({
          quantity_selection : e['value']
        })
        this.getVersions(this.state.sub_recipe_selection, e)
      }
    }

    handleVersionSelect = (e) => {
      if(e!=null){
        cookies.set('version', e['value'], { path: '/' });
        this.setState({
          version_selection : e['value']
        })
        console.log(e)
      }
    }

    handleStartRecording = () => {
      window.location.assign('/home');
    }

    handleProcessesLogs = () => {
      window.location.assign('/main/get-zip');
    }

    handleRecipeModalOpen = () => {
      console.log("inside recipe modal open")
      this.setState({
        recipe_modal_open : true
      })
      console.log(this.state.recipe_modal_open)
    }

    handleIngredientModalOpen = () => {
      console.log("inside ingredient modal open")
      this.setState({
        ingredient_modal_open : true
      })
      console.log(this.state.ingredient_modal_open)
    }

    handleModalClose = () => {
      console.log("inside modal close")
      this.setState({
        ingredient_modal_open : false,
        recipe_modal_open : false,
      })
    }

    handleIngredientModalAdd = (e) => {
      this.handleIngredientSelect(
          {
            'value':this.state.ingredient_modal_sub_recipe,
            'weight': this.state.ingredient_modal_quantity,
          }
        )
      this.handleModalClose()
    }

    handleRecipeModalAdd = () => {
      if (
          window.confirm(
            'Are you sure you want to create a new subrecipe '
            + ' called ' + this.state.recipe_modal_sub_recipe
            + ' which has a quantity of ' + this.state.recipe_modal_quantity
            + ' ?')
        
        ) {
          var data = {
            "recipe": this.state.recipe_modal_sub_recipe,
            "quantity": this.state.recipe_modal_quantity}
          $.getJSON('http://15.206.63.149:7002/create_subrecipe',data)
          console.log("new subrecipe created")

          console.log("init called")   
          this.setState(
            {
              sub_recipe_selection:"",
              quantity_selection:"",
              version_selection:""
            }
          )
          //  init will always fail because of
          $.ajax({
            type: "GET",
            url: "http://15.206.63.149:7002/init",
            crossDomain: true,
            error: () => {
              
              this.getSubRecipes({"value":"all"});
              this.handleSubRecipeSelect({"value": this.state.recipe_modal_sub_recipe});
              
            }
          })
      }

      this.handleModalClose()
      setTimeout(() => { this.getSubRecipes({"value":"all"}); }, 2000);
    }

    handleRowSelected = (state) => {
      this.state.selectedRows = state.selectedRows
    }

    handleDelete = () => {
      this.setState({
        data:differenceBy(this.state.data, this.state.selectedRows, 'ingredient'),
      })
    }

    getSearchableList(arr){
      var new_arr = []
      for(const x of arr){
        new_arr.push(
            {
              'value':x,
              'label':x
            }
          )
      }
      return new_arr
    }

    getSearchableSubRecipeList(arr){
      var new_arr = []
      for(const x of arr){
        new_arr.push(
            {
              'value': "SUB_RECIPE_"+x,
              'label':x
            }
          )
      }
      return new_arr
    }

    getBrands(){     
      console.log("get brands called")
      $.getJSON('http://15.206.63.149:7002/get_brands')
      .then( (res) => {    
          this.setState({
              brand_options : res['brands'],
              brand_searchable_options: this.getSearchableList(res['brands'])
          })          
        }
      );
    }

    getRecipes(data=null){
      console.log("get recipes called")
      if(data){
        $.getJSON('http://15.206.63.149:7002/get_recipes',data={"brand":data['value']})
        .then( (res) => {
            this.setState({
                recipe_options : res['recipes'],
                recipe_searchable_options: this.getSearchableList(res['recipes'])
            })
          }
        );
      }
      else{
        $.getJSON('http://15.206.63.149:7002/get_recipes',)
        .then( (res) => {
            this.setState({
                recipe_options : res['recipes'],
                recipe_searchable_options: this.getSearchableList(res['recipes'])
            })
          }
        );
      }
    }

    getSubRecipes(data=null){
      console.log("get sub recipes called")
      $.getJSON('http://15.206.63.149:7002/get_subrecipes',data={"recipe_name":data['value']})
      .then( (res) => {
          this.setState({
            sub_recipe_options : res['recipes'],
            sub_recipe_searchable_options: this.getSearchableList(res['recipes'])
          })
        }
      );
    }

    getIngredients(data=null){
      console.log("get ingredients called")
      $.getJSON('http://15.206.63.149:7002/get_ingredients',data={"recipe_name":data['value']})
      .then( (res) => {

        var new_options = []
        new_options.push(
          {
            "label": "Add Ingredients ",
            "options": this.getSearchableList(res['ingredients'])
          }
        )

        new_options.push(
          {
            "label": "Search Recipes ",
            "options": this.getSearchableSubRecipeList(this.state.sub_recipe_options)
          }
        )
          this.setState({
            ingredient_options : res['ingredients'],
            ingredient_searchable_options: new_options
          })
        }
      );
    }

    getQuantity(data=null){
      console.log("get quantity called")
      console.log(data)
      $.getJSON('http://15.206.63.149:7002/get_quantities',data={"recipe_name":data['value']})
      .then( (res) => {
          this.setState({
            qty_options : res['quantities'],
            quantity_searchable_options: this.getSearchableList(res['quantities']),
            quantity_selection: res['quantities'][res['quantities'].length-1]
          })
          cookies.set('quantity', res['quantities'][res['quantities'].length-1], { path: '/' });
          this.getVersions(this.state.sub_recipe_selection,{'value':this.state.quantity_selection})
        }
      );
    }

    getVersions(recipe_name, data){
      console.log("get version called")
      console.log(recipe_name)
      console.log(data)
      $.getJSON('http://15.206.63.149:7002/get_versions',data={"recipe_name": recipe_name, "quantity": data['value']})
      .then( (res) => {
          this.setState({
            version_options : res['versions'],
            version_searchable_options: this.getSearchableList(res['versions']),
            version_selection: res['versions'][res['versions'].length-1],
          })
          cookies.set('version', res['versions'][res['versions'].length-1], { path: '/' });
        }
      );
    }

    componentDidMount(){
      this.getBrands();
      this.getRecipes();
      this.getSubRecipes({"value":"all"});
      this.getIngredients({"value":"all"});
    }

    render() {
      return (
          <>
        <Container fluid>
            <Navbar expand="lg" variant="primary" bg="light">
                <Container>
                    <Navbar.Brand href="#">Recorder App</Navbar.Brand>
                </Container>
            </Navbar>
        </Container>

        <Container fluid>
            <Row className='mt-5'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.brand_searchable_options, this.state.brand_selection, this.handleBrandSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.recipe_searchable_options, this.state.recipe_selection, this.handleRecipeSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>
            
            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.sub_recipe_searchable_options,  this.state.sub_recipe_selection, this.handleSubRecipeSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.quantity_searchable_options,  this.state.quantity_selection, this.handleQuantitySelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.version_searchable_options,  this.state.version_selection, this.handleVersionSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderButton("Create New Sub Recipe", "create_new_recipe",this.handleRecipeModalOpen)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderButton("Start Recording Session", "start_recording",this.handleStartRecording)}
            </Col>
            <Col>
            {this.renderButton("Get Processed Recording Logs", "get_processed_logs",this.handleProcessesLogs)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>
            
        </Container>
        {this.renderIngredientModal(this.state.ingredient_modal_open)}
        {this.renderRecipeModal(this.state.recipe_modal_open)}
        </>
      );
    }


  }
  
  ReactDOM.render(
    <Webpage/>,
    document.getElementById('root')
  );
  