import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import './index.scss';

import differenceBy from 'lodash/differenceBy';

import Select from 'react-select'
import makeAnimated from 'react-select/animated';
import DataTable from 'react-data-table-component';

import FloatingLabel from 'react-bootstrap/esm/FloatingLabel';
import Modal from 'react-bootstrap/Modal'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Navbar from 'react-bootstrap/Navbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import InputGroup from 'react-bootstrap/InputGroup'
import Image from 'react-bootstrap/Image'
import { read } from '@popperjs/core';

const animatedComponents = makeAnimated();

const groupStyles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
};
const groupBadgeStyles = {
  backgroundColor: '#EBECF0',
  borderRadius: '2em',
  color: '#172B4D',
  display: 'inline-block',
  fontSize: 12,
  fontWeight: 'normal',
  lineHeight: '1',
  minWidth: 1,
  padding: '0.16666666666667em 0.5em',
  textAlign: 'center',
};

const formatGroupLabel = data => (
  <div style={groupStyles}>
    <span>{data.label}</span>
    <span style={groupBadgeStyles}>{data.options.length}</span>
  </div>
);

class Webpage extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            brand_options : ['KFC', 'Break', 'Mcdonalds', 'Bruh'],
            recipe_options : ['butter chicken', 'tikka', 'veg', 'humans'],
            ingredient_options : ['carrot', 'potato', 'peas', 'cranium'],
            sub_recipe_options : ['garlic', 'ginger', 'break', 'lol'],
            qty_options : ['100 gms', '200 gms'],
            version_options: [],
            ingredient_and_weights : [ ['', 0] ],
            
            setData: false,
            add_to_output: false,
          
            brand_searchable_options : [],
            recipe_searchable_options : [],
            sub_recipe_searchable_options : [] ,
            ingredient_searchable_options : [],
            quantity_searchable_options : [],
            version_searchable_options : [],
          

            input_columns : [
                {
                    name: 'Sr. No',
                    selector: row => row.srno
                },
                {
                    name: 'Ingredient',
                    selector: row => row.ingredient
                },
                {
                    name: 'Qty',
                    selector: row => row.qty,
                    cell:(row) => (
                      <Form.Control type="number" defaultValue={row.qty} onChange={e => row.qty=e.target.value}
                      
                        // onChange={
                        // (e) => {
                        //     this.handleTableChange(e, row.id, "input", "qty")
                        //   }
                        // }
                        />
                    )
                },
                {
                    name: 'UOM',
                    selector: row => row.uom,
                    cell:(row) => (
                      <Form.Control type="text" defaultValue={row.uom} onChange={e => row.uom=e.target.value}/>
                    )
                },
                {
                  name: 'Comment',
                  selector: row => row.comment,
                  cell:(row) => (
                    <Form.Control type="text" defaultValue={row.comment} onChange={e => row.comment=e.target.value}/>
                  )
                },
                {
                  cell: row => (
                    <Button
                      aria-label="Edit"
                      color="secondary"
                      onClick={() => this.handleEdit(row.id,"input")}
                    >  
                    Edit
                    </Button>
                  )
                },
                {
                  cell: row => (
                    <Button
                      aria-label="delete"
                      color="secondary"
                      onClick={() => this.handleDelete(row.id, 'input')}
                    >  
                    Delete
                    </Button>
                  )
                }
            ],

          // data : [{id: 0, srno:'1', ingredient: 'Enter', qty: '100', uom: 'g', comment: 'This'}],
          data : [],

          output_columns : [
            {
                name: 'Sr. No',
                selector: row => row.srno
            },
            {
                name: 'Output Name',
                selector: row => row.ingredient
            },
            {
                name: 'Qty',
                selector: row => row.qty,
                cell:(row) => (
                  <Form.Control type="number" defaultValue={row.qty} onChange={e => row.qty=e.target.value}/>
                )
            },
            {
              name: 'Qty in pieces',
              selector: row => row.qty_pc,
              cell:(row) => (
                <Form.Control type="number" defaultValue={row.qty_pc} onChange={e => row.qty_pc=e.target.value}/>
              )
            },
            {
              name: 'Comment',
              selector: row => row.comment,
              cell:(row) => (
                <Form.Control type="text" defaultValue={row.comment} onChange={e => row.comment=e.target.value}/>
              )
            },
            {
              cell: row => (
                <Button
                  aria-label="Edit"
                  color="secondary"
                  onClick={() => this.handleEdit(row.id,"output")}
                >  
                Edit
                </Button>
              )
            },
            {
              cell: row => (
                <Button
                  aria-label="delete"
                  color="secondary"
                  onClick={() => this.handleDelete(row.id, 'output')}
                >  
                Delete
                </Button>
              )
            }
          ],
          output_data : [],

          cur_table_id : 0,

          ingredient_modal_open : false,
          ingredient_modal_sub_recipe : "",
          ingredient_modal_quantity : "",

          recipe_modal_open : false,  
          recipe_modal_sub_recipe: "",        
          recipe_modal_quantity: "",  

          edit_modal_open: false,
          edit_modal_sub_recipe: "",
          row_to_edit: "",
          edit_type: "",

          brand_selection: "Brand",
          recipe_selection: "Recipe",
          sub_recipe_selection: "Sub Recipe",
          quantity_selection: "Quantity",
          version_selection: "Version",
          output_weight: 0,
          ingredient_selected_options: [],
          user_image: "",
          user_name: "",
          old_pos: "Old Position",
          new_pos: "New Position",
          show_images: true,
          shared_comment: ""
        }
    }

    renderIngredientModal(variable){
      return (
      <Modal show={variable} onHide={this.handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Component</Modal.Title>
        </Modal.Header>
        

        <FloatingLabel controlId="Ingredient" label="Ingredient">
          <Form.Control type="text" placeholder="Ingredient" onChange={e => this.setState({ ingredient_modal_sub_recipe: e.target.value })}/>
        </FloatingLabel>

        <FloatingLabel controlId="Quantity" label="Quantity">
          <Form.Control type="number" placeholder="Quantity" onChange={e => 
            this.setState({ 
              ingredient_modal_quantity: e.target.value
            })}/>
        </FloatingLabel>

          <Button variant="secondary" onClick={this.handleModalClose} >
            Close
          </Button>
          <Button variant="primary" onClick={this.handleIngredientModalAdd} >
            Add
          </Button>
      </Modal>
      );
    }

    renderEditModal(variable){
      return (
        <Modal show={variable} onHide={this.handleModalClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Ingredient</Modal.Title>
          </Modal.Header>
  
          <Row className='mt-3'>
          <Col xs={1}>
          </Col>
          <Col>
          {this.renderSearch(this.getSearchableList(this.state.ingredient_options), "Ingredients", this.handleEditModalIngredientSelect.bind(this))}
          </Col>
          <Col xs={1}>
          </Col>
          </Row>        
          <br>
          </br>
  
          <Button variant="secondary" onClick={this.handleModalClose} fluid>
            Close
          </Button>
          <Button variant="primary" onClick={this.handleEditModalAdd} fluid>
            Edit
          </Button>
        </Modal>
        );
    }

    renderRecipeModal(variable){
      return (
      <Modal show={variable} onHide={this.handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Recipe To DB</Modal.Title>
        </Modal.Header>

        <Modal.Body className="show-grid">
          <Container>
            <Row>
              <Col xs={12} md={6}>
                {
                  this.renderSearch(this.state.sub_recipe_searchable_options, "Mod Existing Recipe", this.handleNewRecipeSelect)
                }
              </Col>
              <Col xs={12} md={6}>
                <FloatingLabel controlId="SubRecipe" label="SubRecipe">
                  <Form.Control type="text" value={this.state.recipe_modal_sub_recipe} onChange={e => this.setState({ recipe_modal_sub_recipe: e.target.value })}/>
                </FloatingLabel>
              </Col>
            </Row>

            <Row>
              <Col xs={12} md={6}>
                {
                  this.renderSearch(this.state.quantity_searchable_options, "Mod Existing Qty", this.handleNewQtySelect)
                }
              </Col>
              <Col xs={12} md={6}>
              <FloatingLabel controlId="Quantity" label="Quantity">
                <Form.Control type="number" value={this.state.recipe_modal_quantity} onChange={e => this.setState({ recipe_modal_quantity: e.target.value })}/>
              </FloatingLabel>
              </Col>
            </Row>
          </Container>
        </Modal.Body>



        <Button variant="secondary" onClick={this.handleModalClose} fluid>
          Close
        </Button>
        <Button variant="primary" onClick={this.handleRecipeModalAdd} fluid>
          Add
        </Button>
      </Modal>
      );
    }

    renderGreatTable(title, data, columns){
      return (
        <DataTable
          title={title}
          columns={columns}
          data={data}
          selectableRows
          contextComponent={
            <Button key="delete" onClick={this.handleDelete} style={{ backgroundColor: 'red' }}>
              Delete
            </Button>
          }
          onSelectedRowsChange={this.handleRowSelected}
        />
      );

    }

    renderButton(text, id, onClick){
        if(onClick!=='null'){
            return(
              <ButtonGroup className="d-flex">
                <Button variant="primary" id={id} onClick={onClick}>{text}</Button>
              </ButtonGroup>
                );
        }
        else{
            return(
                <ButtonGroup className="d-flex">
                  <Button variant="primary" id={id} >{text}</Button>
                </ButtonGroup>
                );
        }
    }

    renderIngredientSearch(options, placeholder, onChange){
      return(
          <Select 
            options={options}
            components={animatedComponents}
            isClearable={true}
            isMulti
            placeholder={placeholder}
            defaultValue={this.state.ingredient_selected_options}
            value={this.state.ingredient_selected_options}
            onChange={onChange}
            closeMenuOnSelect={true}
            formatGroupLabel={formatGroupLabel}
          />
      );
    }

    renderSearch(options, placeholder, onChange){
      return(
          <Select 
            options={options}
            isClearable={true}
            placeholder={placeholder}
            onChange={onChange}
          />
      );
    }

    handleBrandSelect = (e) => {
      if(e!=null){
        this.getRecipes(e)
      }
    }

    handleRecipeSelect = (e) => {
      if(e!=null){
        this.getSubRecipes(e)
      }
    }

    handleSubRecipeSelect = (e) => {
      if(e!=null){
        console.log("handle sub recipe called")
        this.setState({
          sub_recipe_selection : e['value']
        })
        this.getQuantity(e)
      }
    }

    handleNewRecipeSelect = (e) => {
      if(e!=null){
        console.log("handle new sub recipe called")
        this.setState({
          recipe_modal_sub_recipe : e['value']
        })
        this.getQuantity(e)
      }
    }

    handleNewQtySelect = (e) => {
      if(e!=null){
        console.log("handle new qty called")
        this.setState({
          recipe_modal_quantity : e['value']
        })
      }
    }

    handleQuantitySelect = (e) => {
      console.log('inside quantity select')
      if(e!=null){
        this.setState({
          quantity_selection : e['value']
        })
        this.getVersions(this.state.sub_recipe_selection, e)
      }
    }

    handleVersionSelect = (e) => {
      if(e!=null){
        this.setState({
          version_selection : e['value']
        })
        console.log(e)
      }
    }

    handleOutputIngredientSelect = (e) =>{
      console.log('inside handle output quantity select select')
      console.log(e)
      this.handleIngredientSelect(e, "output")
    }

    handleIngredientSelect = (e, type="input") => {
      console.log("ingredient select")        
      if(e===null || e===[] || e.length === 0){
        this.setState({
          ingredient_selected_options : e
        }) 
        this.getIngredients()
      }
      else if(e.length!==0){
          var new_e = e.slice(-1)
          if(new_e[0]['value'].startsWith('SUB_RECIPE_')){          
            this.setState({
              ingredient_selected_options : e
            }) 
            // Update searchable options
            var new_options = []
            var data={"recipe_name":new_e[0]['label']}
  
            $.getJSON('http://15.206.63.149:7002/get_ingredients',data)
            .then( (res) => {
                $.getJSON('http://15.206.63.149:7002/get_subrecipes',data)
                .then( (sub_res) => {
                  new_options = res['ingredients'].concat(sub_res['recipes'])
                  this.setState({
                    ingredient_searchable_options: this.getSearchableList(new_options),            
                  })
                  console.log("ingredient set")
                  console.log(this.state.ingredient_searchable_options)
                  console.log(this.state.sub_recipe_searchable_options)
                  }
                );
              }
            );

          }
          else{         
            if(new_e[0]['weight']==null){
              new_e[0]['weight']=0
            }
            console.log("Adding to table")
            console.log(type)   
            console.log(e)    
            if(type==="output"){
              console.log("adding to output")
              console.log(new_e[0])
              var new_data = this.state.output_data
              new_data.push(
                {
                    id: this.state.cur_table_id+1,
                    srno: new_data.length+1,
                    ingredient: new_e[0]['label'],
                    qty: parseFloat(new_e[0]['weight']),
                    qty_pc: '',
                    comment: ''
                },
              )

              this.setState(
                {
                  output_data: new_data
                }
              )
            }
            else{
              console.log("adding to input")
              console.log(new_e)      
              var new_data = this.state.data
              new_data.push(
                {
                    id: this.state.cur_table_id+1,
                    srno: new_data.length+1,
                    ingredient: new_e[0]['label'],
                    qty: parseFloat(new_e[0]['weight']),
                    uom: 'g',
                    comment: ''
                },
              )
              this.setState(
                {
                  data: new_data
                }
              )
            }             
      
            this.setState({
              cur_table_id : this.state.cur_table_id + 1,
              ingredient_selected_options : []
            })    
          }
        
      }
    }

    handleEditModalIngredientSelect = (e) => {
      console.log("Ingredient Selected in edit modal")
      
      this.setState({
        edit_modal_sub_recipe: e['label']
      },() => { this.handleEditModalAdd() })

    }

    

    handleLoadRecipe = () => {
      console.log("load recipe called")
      var temp_data = {"recipe": this.state.sub_recipe_selection,"quantity": this.state.quantity_selection,"version": this.state.version_selection}
      $.getJSON('http://15.206.63.149:7002/get_saved_subrecipe',temp_data)
      .then( (res) => {
        console.log(res)
          var new_data = []
          var parse_data = JSON.parse(res['state_text'])
          console.log("This is input data")
          console.log(parse_data)
          for(const x in parse_data){
            parse_data[x]['id'] = this.state.cur_table_id + 1
            new_data.push(
              parse_data[x]
            )
            this.setState({
              cur_table_id : this.state.cur_table_id + 1
            })
          }

          var new_output_data = []
          parse_data = JSON.parse(res['output_state_text'])
          console.log("This is output data")
          console.log(parse_data)
          for(const x in parse_data){
            parse_data[x]['id'] = this.state.cur_table_id + 1
            new_output_data.push(
              parse_data[x]
            )

            // new_output_data.push(
            //   {
            //       id: this.state.cur_table_id + 1,
            //       srno: new_output_data.length+1,
            //       ingredient: parse_data[x]['ingredient'],
            //       weight: parse_data[x]['weight'],
            //       uom: parse_data[x]['uom'],
            //       qty: parse_data[x]['qty'],
            //       qty_pc: parse_data[x]['qty_pc'],
            //       comment: parse_data[x]['comment'],
            //   }
            // )
            this.setState({
              cur_table_id : this.state.cur_table_id + 1
            })
          }
          
          if(new_data===[]){
            new_data = [{id: 0, ingredient: 'Enter', weight: '100', uom: 'g', qty: '1', comment: ''}]
          }
          if(new_output_data===[]){
            new_output_data = [{id: 0, ingredient: 'Enter', weight: '100', uom: 'g', qty: '1', qty_pc: '1', comment: ''}]
          }
          console.log(new_data)
          this.setState({
              data : new_data,
              output_data : new_output_data,
              output_weight: res['output_weight'],
              user_image:res['image'],
              user_name: res['user_name'],
              shared_comment: res['shared_comment']
          })
        }
      );
      
    }

    handleSave = () => {
      if(this.state.sub_recipe_selection==="Sub Recipe" || this.state.sub_recipe_selection===""){
        window.confirm("Please select a sub recipe")
      }
      else if(this.state.quantity_selection==="Quantity" || this.state.quantity_selection===""){
        window.confirm("Please select a quantity")
      }
      else if(this.state.user_image==="Add Image Links" || this.state.user_image===""){
        window.confirm("Please add a photo of yourself")
      }
      else if(this.state.user_name===""|| this.state.user_name===""){
        window.confirm("Please add your name")
      }
      else{
        console.log(this.state.data)
        var data =  {
          "recipe": this.state.sub_recipe_selection,
          "quantity": this.state.quantity_selection,
          "version": this.state.version_selection,
          "state": JSON.stringify(this.state.data),
          "output_state": JSON.stringify(this.state.output_data),
          "output_weight": this.state.output_weight,
          "user_name": this.state.user_name,
          "image": this.state.user_image,
          "shared_comment": this.state.shared_comment,
        }
        $.ajax({
          type: "POST",
          url: "http://15.206.63.149:7002/save_subrecipe_state",
          data: data,
        })
        .then( (res) => {
          console.log("recipe saved")
        }
        );
        window.confirm('Ingredients and Image (If Available) saved')
      }
    }

    handleRecipeModalOpen = () => {
      console.log("inside recipe modal open")
      this.setState({
        recipe_modal_open : true
      })
      console.log(this.state.recipe_modal_open)
    }

    handleIngredientModalOpen = (text) => {
      console.log("inside ingredient modal open")
      this.setState({
        ingredient_modal_open : true,
        add_to_output: "input"
      })
      console.log(this.state.ingredient_modal_open)
    }

    handleOutputIngredientModalOpen = () => {
      console.log("inside ingredient modal open")
      this.setState({
        ingredient_modal_open : true,
        add_to_output: "output"
      })
      console.log(this.state.ingredient_modal_open)
    }

    handleModalClose = () => {
      console.log("inside modal close")
      this.setState({
        ingredient_modal_open : false,
        recipe_modal_open : false,
        edit_modal_open : false,
      })
    }

    handleIngredientModalAdd = (e) => {
      this.handleIngredientSelect(
          [{
            'value': "CUSTOM_INGREDIENT_" + this.state.ingredient_modal_sub_recipe,
            'label': '*' + this.state.ingredient_modal_sub_recipe,
            'weight': this.state.ingredient_modal_quantity.toString(),
          }],
          this.state.add_to_output
        )
      this.handleModalClose()
    }

    handleRecipeModalAdd = () => {      
      if(this.state.recipe_modal_sub_recipe===""){
        window.confirm("Please Add A Sub Recipe")
      }
      else if(this.state.recipe_modal_quantity===""){
        window.confirm("Please Add A Quantity")
      }
      else if (
          window.confirm(
            'Are You Sure You Want To Create A New Subrecipe '
            + ' called ' + this.state.recipe_modal_sub_recipe
            + ' which has a quantity of ' + this.state.recipe_modal_quantity
            + ' ?')
        
        ) {
          var data = {
            "recipe": this.state.recipe_modal_sub_recipe,
            "quantity": this.state.recipe_modal_quantity}
          $.getJSON('http://15.206.63.149:7002/create_subrecipe',data)
          console.log("new subrecipe created")
          this.setState(
            {
              sub_recipe_selection : this.state.recipe_modal_sub_recipe,
              quantity_selection : this.state.recipe_modal_quantity,
            }
          )
          this.setState(
            {
              sub_recipe_selection:"",
              quantity_selection:"",
              version_selection:""
            }
          )

          console.log("init called")   
          //  init will always fail because of
          $.ajax({
            type: "GET",
            url: "http://15.206.63.149:7002/init",
            crossDomain: true,
            error: () => {
              
              this.getSubRecipes({"value":"all"});
              this.handleSubRecipeSelect({"value": this.state.recipe_modal_sub_recipe});
              
            }
          })      
      }

      this.handleModalClose()
    }

    handleEditModalAdd = () => { 
      console.log("handle edit modal add called")
      console.log(this.state.row_to_edit)
      
      var new_data = []
      var data_to_edit = null
      if(this.state.edit_type==="input"){
        data_to_edit = this.state.data
      }
      else{
        data_to_edit = this.state.output_data
      }

      console.log("inside edit modal")

      for(const x of data_to_edit){
        console.log(x)
        if(x['id']===this.state.row_to_edit){
          console.log(x)
          x['ingredient'] = this.state.edit_modal_sub_recipe
          x['id']=this.state.cur_table_id+1
        }
        new_data.push(x)
      }
      console.log("This is new data")
      console.log(this.state.edit_modal_sub_recipe)
      console.log(new_data)
      if(this.state.edit_type==="input"){
        this.setState({
          data: new_data,
          cur_table_id: this.state.cur_table_id+1
        })
      }
      else{
        this.setState({
          output_data: new_data,
          cur_table_id: this.state.cur_table_id+1
        })
      }

      this.handleModalClose()
    }

   
    handleRowSelected = (state) => {
      this.setState({
        selectedRows : state.selectedRows
      })
    }

    handleDelete = (e, type="input") => {
      console.log("handle delete called")
      console.log(e)
      var new_data = []

      var data_to_change = null

      if(type==="input"){
        data_to_change = this.state.data
      }
      else{
        data_to_change = this.state.output_data
      }
      var srno = 1
      for(const x of data_to_change){
        console.log(x)
        if(x['id']!==e){
          x['srno']=srno
          new_data.push(x)
          srno += 1
        }
      }

      console.log("This is new data")
      console.log(new_data)

      if(type==="input"){
        this.setState({
          data: new_data
        })
      }
      else{
        this.setState({
          output_data: new_data
        })
      }
    }

    handleEdit = (e, type) => {
      console.log("handle edit called")

      this.setState({
        row_to_edit:e,
        edit_type:type,
        edit_modal_open: true
      })

      console.log(this.state.edit_modal_open)
      console.log(this.state.row_to_edit)
      console.log(this.state.edit_type)
    }

    handleTableChange = (e, row, type, field) => {
      console.log(e.target.value)
      console.log(row)
      console.log(type)
      console.log(field)

      console.log("handle table change called")      
      var new_data = []
      var data_to_edit = null
      if(type==="input"){
        data_to_edit = this.state.data
      }
      else{
        data_to_edit = this.state.output_data
      }

      console.log("inside edit modal")

      for(const x of data_to_edit){
        console.log(x)
        if(x['id']===row){
          x['id']=this.state.cur_table_id+1
          x['qty']= e.target.value
        }
        new_data.push(x)
      }
      console.log("This is new data")
      console.log(new_data)

      // if(type==="input"){
      //   this.setState({
      //     data: new_data,
      //     cur_table_id: this.state.cur_table_id+1
      //   })
      // }
      // else{
      //   this.setState({
      //     output_data: new_data,
      //     cur_table_id: this.state.cur_table_id+1
      //   })
      // }

      
    }
    

    handleReorder = (e) => {
      var old_position = parseInt(this.state.old_pos)
      var new_position = parseInt(this.state.new_pos)

      if(old_position > this.state.data.length || new_position > this.state.data.length){
        window.confirm("Positions are out of bound")
      }
      else{
        console.log(old_position)
        console.log(new_position)
        var new_order = []
        var initial_order = this.state.data
        var x = old_position
        var y = new_position
        if (x < y) {
          console.log("x<y case")
          for (let i = 1; i <= initial_order.length; i++) {
            if (i < x) {
              new_order.push(initial_order[i-1])
            }
            else if (i >= x) {
              if(i < y) {
                new_order.push(initial_order[i])
              }
              else if(i === y) {
                new_order.push(initial_order[x-1])
              }
              else {
                new_order.push(initial_order[i-1])
              }
            }
          }
        }
        else if (x > y) {
          console.log("x>y case")
          for (let i = 1; i <= initial_order.length; i++) {
            if (i < y) {
              new_order.push(initial_order[i-1])
            }
            else if (i >= y) {
              if(i === y) {
                new_order.push(initial_order[x-1])
              }
              else if(i <= x) {
                new_order.push(initial_order[i - 2])
              }
              else {
                new_order.push(initial_order[i-1])
              }
            }
          }
        }
        else {
          new_order = initial_order
        }

        for(x in new_order){
          new_order[x]['srno'] = parseInt(x) +1
        }

        console.log(new_order)

        this.setState({
          data: new_order,
        })
      }
    }

    getSearchableList(arr){
      var new_arr = []
      for(const x of arr){
        new_arr.push(
            {
              'value':x,
              'label':x
            }
          )
      }
      return new_arr
    }

    getSearchableSubRecipeList(arr){
      var new_arr = []
      for(const x of arr){
        new_arr.push(
            {
              'value': "SUB_RECIPE_"+x,
              'label':x
            }
          )
      }
      return new_arr
    }

    getBrands(){     
      console.log("get brands called")
      $.getJSON('http://15.206.63.149:7002/get_brands')
      .then( (res) => {    
          this.setState({
              brand_options : res['brands'],
              brand_searchable_options: this.getSearchableList(res['brands'])
          })          
        }
      );
    }

    getRecipes(data=null){
      console.log("get recipes called")
      if(data){
        $.getJSON('http://15.206.63.149:7002/get_recipes',data={"brand":data['value']})
        .then( (res) => {
            this.setState({
                recipe_options : res['recipes'],
                recipe_searchable_options: this.getSearchableList(res['recipes'])
            })
          }
        );
      }
      else{
        $.getJSON('http://15.206.63.149:7002/get_recipes',)
        .then( (res) => {
            this.setState({
                recipe_options : res['recipes'],
                recipe_searchable_options: this.getSearchableList(res['recipes'])
            })
          }
        );
      }
    }

    getSubRecipes(data=null){
      console.log("get sub recipes called")
      $.getJSON('http://15.206.63.149:7002/get_subrecipes',data={"recipe_name":data['value']})
      .then( (res) => {
          this.setState({
            sub_recipe_options : res['recipes'],
            sub_recipe_searchable_options: this.getSearchableList(res['recipes'])
          })
        }
      );
    }

    getOItems(data){
      $.getJSON('http://15.206.63.149:7002/get_subrecipes',data={"recipe_name":data['value']})
      .then( (res) => {

        var new_options = []
        new_options.push(
          {
            "label": "Add Ingredients ",
            "options": this.getSearchableList(res['ingredients'])
          }
        )

        new_options.push(
          {
            "label": "Search Recipes ",
            "options": this.getSearchableSubRecipeList(this.state.sub_recipe_options)
          }
        )


          this.setState({
            ingredient_options : res['ingredients'],
            ingredient_searchable_options: new_options
          })
        }
      );
    }

    getIngredients(data){
      console.log("get ingredients called")
      var new_options = []
        $.getJSON('http://15.206.63.149:7002/get_ingredients',data={"recipe_name":"all"})
        .then( (res) => {
            $.getJSON('http://15.206.63.149:7002/get_subrecipes',data={"recipe_name":"init"})
            .then( (sub_res) => {

              new_options.push(
                {
                  "label": "Add Ingredients ",
                  "options": this.getSearchableList(res['ingredients'])
                }
              )
              new_options.push(
                {
                  "label": "Search Recipes ",
                  "options": this.getSearchableSubRecipeList(sub_res['recipes'])
                }
              )

              this.setState({
                ingredient_options : res['ingredients'],
                ingredient_searchable_options: new_options
              })
            }
            );
          }
        );
    }

    getQuantity(data=null){
      console.log("get quantity called")
      console.log(data)
      $.getJSON('http://15.206.63.149:7002/get_quantities',data={"recipe_name":data['value']})
      .then( (res) => {
          this.setState({
            qty_options : res['quantities'],
            quantity_searchable_options: this.getSearchableList(res['quantities']),
            quantity_selection: res['quantities'][res['quantities'].length-1]
          })
          this.getVersions(this.state.sub_recipe_selection,{'value': res['quantities'][res['quantities'].length-1]})
        }
      );
    }

    getVersions(recipe_name, data){
      console.log("get version called")
      console.log(recipe_name)
      console.log(data)
      $.getJSON('http://15.206.63.149:7002/get_versions',data={"recipe_name": recipe_name, "quantity": data['value']})
      .then( (res) => {
        console.log(res)
          

          this.setState({
            version_options : res['versions'],
            version_searchable_options: this.getSearchableList(res['versions']),
            version_selection: res['versions'][res['versions'].length-1],
          })

          if(!this.state.recipe_modal_open){
            window.confirm("New Subrecipe Ready To Be Edited")
          }          
        }
      );
    }

    componentDidMount(){
      this.getBrands();
      this.getRecipes();
      this.getSubRecipes({"value":"all"});
      this.getOItems({"value":"init"});
      this.getIngredients();
    }

    render() {
      return (
          <>
        <Container fluid>
            <Navbar expand="lg" variant="primary" bg="light">
                <Container>
                    <Navbar.Brand href="#">Ingredient App</Navbar.Brand>
                </Container>
            </Navbar>
        </Container>

        <Container fluid>
            <Row className='mt-5'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.brand_searchable_options, this.state.brand_selection, this.handleBrandSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.recipe_searchable_options,this.state.recipe_selection, this.handleRecipeSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>
            
            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.sub_recipe_searchable_options, this.state.sub_recipe_selection, this.handleSubRecipeSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.quantity_searchable_options, this.state.quantity_selection, this.handleQuantitySelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderSearch(this.state.version_searchable_options, this.state.version_selection, this.handleVersionSelect)}
            </Col>
            <Col>
            {this.renderButton("Load Recipe", "load_recipe",this.handleLoadRecipe)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderButton("Create New Sub Recipe", "create_new_recipe",this.handleRecipeModalOpen)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
              <Form.Control type="Number" placeholder={this.state.old_pos} onChange={e => this.setState({ old_pos: e.target.value })}/>
            </Col>
            <Col>
              <Form.Control type="Number" placeholder={this.state.new_pos} onChange={e => this.setState({ new_pos: e.target.value })}/>
            </Col>
            <Col>
            {this.renderButton("Reorder", "reorder",this.handleReorder)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderIngredientSearch(this.state.ingredient_searchable_options, "Ingredients", this.handleIngredientSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderButton("Add New Ingredient To Input Table", "new_ingredient",this.handleIngredientModalOpen)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>   

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col xs={10}>
            {this.renderGreatTable("Input Ingredients",this.state.data, this.state.input_columns)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>


            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderIngredientSearch(this.state.ingredient_searchable_options, "Output Ingredients", this.handleOutputIngredientSelect)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>
            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col xs={10}>
            {this.renderGreatTable("Output Ingredients",this.state.output_data, this.state.output_columns)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={1}>
            </Col>
            <Col>
            {this.renderButton("Add New Ingredient To Output Table", "new_ingredient_output",this.handleOutputIngredientModalOpen)}
            </Col>
            <Col xs={1}>
            </Col>
            </Row> 

            <Row className='mt-3'>
            <Col xs={3}>
            </Col>
            <Col>
            <FloatingLabel controlId="user_name" label="User name">
              <Form.Control type="text" value={this.state.user_name} onChange={e => this.setState({ user_name: e.target.value })}/>
            </FloatingLabel>
            </Col>
            <Col xs={3}>
            </Col>
            </Row>
            
            <Row className='mt-3'>
            <Col xs={3}>
            </Col>
            <Col>
            <InputGroup size="lg">
              <InputGroup.Text id="inputGroup-sizing-md">Output Weight</InputGroup.Text>
              <Form.Control type="text" value={this.state.output_weight} onChange={e => this.setState({ output_weight: e.target.value })}/>
            </InputGroup>
            </Col>
            <Col xs={3}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={3}>
            </Col>
            <Col>
            <InputGroup size="lg">
              <InputGroup.Text id="inputGroup-sizing-md">Comments</InputGroup.Text>
              <Form.Control type="textarea" rows={4} value={this.state.shared_comment} onChange={e => this.setState({ shared_comment: e.target.value })}/>
            </InputGroup>
            </Col>
            <Col xs={3}>
            </Col>
            </Row>

            <Row className='mt-3'>
            <Col xs={3}>
            </Col>
            <Col>
            <InputGroup size="lg">
              <InputGroup.Text id="inputGroup-sizing-md">Image Links</InputGroup.Text>
              <Form.Control as="textarea" type="text" rows={4} value={this.state.user_image} onChange={e => this.setState({ user_image: e.target.value })}/>
            </InputGroup>
            </Col>
            <Col xs={3}>
            </Col>
            </Row>
            
            <Row className='mt-3'>
            <Col xs={2}>
            </Col>
            <Col>          
            {this.renderButton("Save Data", "progress_save",this.handleSave)}
            </Col>
            <Col xs={2}>
            </Col>
            </Row>
            
        </Container>
        {this.renderIngredientModal(this.state.ingredient_modal_open)}
        {this.renderRecipeModal(this.state.recipe_modal_open)}
        {this.renderEditModal(this.state.edit_modal_open)}
        </>
      );
    }


  }
  
  ReactDOM.render(
    <Webpage/>,
    document.getElementById('root')
  );
  