#!/bin/bash
cd /app/ingredient/project
gunicorn --bind=0.0.0.0:8000 --workers $NUM_WORKERS --max-requests=1000 -c /app/ingredient/project/docker_gunicorn_configuration.py project.wsgi:application