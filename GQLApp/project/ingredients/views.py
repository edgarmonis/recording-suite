import datetime, json, copy, csv, os
from io import StringIO
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from utils.gql import GqlHelper
import urllib.parse

query_manager = GqlHelper()

brand_oi_map = {}
oi_sub_recipe_map = {}
oi_ingredient_map = {}
oi_main_cook_map = {}

def get_maincook_from_oi(sub_recipe):
    oi = query_manager.getOrderableItem(sub_recipe)
    if oi:        
        mainCook = oi['mainCook']
        oi_main_cook_map[sub_recipe]=mainCook
        return oi_main_cook_map[sub_recipe]
    else:
        False

def get_recursive_sub_recipes(sub_recipe, sr_list):
    ing = query_manager.getIngredient(sub_recipe)
    if ing and ing['recipes']:
        ing_list = []
        for quantity in ing['recipes']:
            ing_list += ing['recipes'][quantity]

        recipe_names = [obj['name'].split('.csv')[0] for obj in ing_list]
        ingredient_from_recipe = []
        for recipe in recipe_names:
            sub_recipe_from_ing = query_manager.getSubrecipeName(recipe)
            
            if sub_recipe_from_ing:
                ingredient_from_recipe += sub_recipe_from_ing['ingredients']

        ingredient_from_recipe = [obj['ingredientName'] for obj in ingredient_from_recipe]
        for ingredient in ingredient_from_recipe:
            if not ingredient in oi_sub_recipe_map.keys():
                get_recursive_sub_recipes(ingredient, [])
            sr_list += oi_sub_recipe_map[ingredient]
        
        sr_list.append(sub_recipe)
        oi_sub_recipe_map[sub_recipe]=sr_list
    else:
        oi_sub_recipe_map[sub_recipe]=[]

def get_recursive_ingredients(sub_recipe, ir_list):
    ing = query_manager.getIngredient(sub_recipe)
    if ing and ing['recipes']:
        ing_list = []
        for quantity in ing['recipes']:
            ing_list += ing['recipes'][quantity]

        recipe_names = [obj['name'].split('.csv')[0] for obj in ing_list]
        ingredient_from_recipe = []
        for recipe in recipe_names:
            sub_recipe_from_ing = query_manager.getSubrecipeName(recipe)
            if sub_recipe_from_ing:
                ingredient_from_recipe += sub_recipe_from_ing['ingredients']

        ingredient_from_recipe = [obj['ingredientName'] for obj in ingredient_from_recipe]
        
        for ingredient in ingredient_from_recipe:
            if not ingredient in oi_ingredient_map.keys():
                get_recursive_ingredients(ingredient, set())
            ir_list.update(oi_ingredient_map[ingredient])
    else:
        ir_list.update([sub_recipe])

    oi_ingredient_map[sub_recipe]=list(ir_list)

@csrf_exempt
def init(request):
    brands_list = query_manager.getMultipleBrands()
    brands = [obj['name'] for obj in brands_list]

    oi_sub_recipe_map['all'] = []
    oi_ingredient_map['all'] = []
    oi_main_cook_map['all']='all'
    for brand in brands:
        recipe_list = query_manager.getOrderableItemsByBrand(brand)
        recipes = [obj['orderableItemName'] for obj in recipe_list]
        brand_oi_map[brand]=recipes
        
        for oi in recipes:            
            main_cook = get_maincook_from_oi(oi)
            get_recursive_sub_recipes(main_cook, [])
            get_recursive_ingredients(main_cook, set())

            oi_sub_recipe_map['all'] += oi_sub_recipe_map[main_cook]
            oi_ingredient_map['all'] += oi_ingredient_map[main_cook]

    oi_sub_recipe_map['all']=sorted(list(set(oi_sub_recipe_map['all'])))
    oi_ingredient_map['all']=sorted(list(set(oi_ingredient_map['all'])))

    return JsonResponse({"message":"done"})

def search_ingredient(request):
    q = request.GET['q']

    if q:
        s = IngredientDocument.search().query("match", ingredient=q)

        results = []
        for hit in s:
            results.append( (hit.ingredient, hit.type1, hit.type2) )
        
        return JsonResponse({"results":results})

def mutate_response(response):
    response["Access-Control-Allow-Origin"] = "*"
    response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
    response["Access-Control-Max-Age"] = "1000"
    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"

    return response

def get_brands(request):
    # 'given brand id'
    brands_list = brand_oi_map.keys()
    brands = [x for x in brands_list]
    response = JsonResponse({"brands":brands})

    return mutate_response(response)
    

def get_recipes(request):
    brand = request.GET.get('brand',None)

    if brand is None:
        recipe_list = []
        for brand in brand_oi_map:
            recipe_list += brand_oi_map[brand]        
        recipes = sorted(recipe_list)
    else:        
        recipe_list = query_manager.getOrderableItemsByBrand(brand)
        recipes = [obj['orderableItemName'] for obj in recipe_list]
        recipes = sorted(recipes)
    
    response = JsonResponse({"recipes":recipes})
    return mutate_response(response)

def get_subrecipes(request):
    recipe_name = request.GET.get('recipe_name',None)
    if recipe_name == "all":
        sub_recipes = query_manager.getSubrecipeAll()
        sub_recipes = [obj['subrecipeName'] for obj in sub_recipes]
    elif recipe_name == "init":
        brands = [x for x in brand_oi_map.keys()]
        sub_recipes = []
        for x in brands:
            sub_recipes.extend(brand_oi_map[x])
        # sub_recipes = [x for x in sub_recipes]
    else:
        main_cook = get_maincook_from_oi(recipe_name)
        sub_recipes = oi_sub_recipe_map[main_cook]

    sub_recipes = sorted(sub_recipes)
    response = JsonResponse({"recipes": sub_recipes})
    return mutate_response(response)


def get_ingredients(request):
    recipe_name = request.GET.get('recipe_name',None)

    if recipe_name == "all":
        ingredient_list = query_manager.getIngredientAll()
        ingredients = sorted([obj['ingredientName'] for obj in ingredient_list])
    else:
        recipe_name = get_maincook_from_oi(recipe_name)
        ingredients = oi_ingredient_map[recipe_name]
        

    response = JsonResponse({"ingredients": ingredients})
    return mutate_response(response)

@csrf_exempt
def create_subrecipes(request):
    brand=request.GET.get('brand')
    subrecipe=request.GET.get('recipe')
    quantity=request.GET.get('quantity')
    # version format is 'vx 12Sep2021'

    # ================================================
    # If subrecipe exists, 
    # Check if same quantity exists
    # if it does create new version and update state

    sub_recipe = query_manager.getSubrecipeName(subrecipe)

    if sub_recipe:
        sub_recipe = query_manager.getSubrecipeName(subrecipe, state=True)
        state = json.loads(sub_recipe['state'])

        if quantity in state['quantities'].keys() and \
            'latest_version' in state['quantities'][quantity].keys():
            latest_version = state['quantities'][quantity]['latest_version']
        else:
            # If same quantity doesnt exist, then create version 1
            latest_version = 0

        latest_version += 1

        new_state = []
        if len(state['quantities']) > 0:
            quantities = sorted([key for key in state['quantities'].keys()])
            base_quantity = quantities[0]
            new_quantity = quantity

            base_versions = sorted([version for version in state['quantities'][base_quantity].keys()])
            base_version = base_versions[1]

            if 'ingredient_app_state' in state['quantities'][base_quantity][base_version].keys():
                base_app_state_text = state['quantities'][base_quantity][base_version]['ingredient_app_state']
                base_app_state = json.loads(base_app_state_text)
                
                for x in base_app_state:
                    if 'qty' in x.keys():
                        key_name = 'qty'
                    if 'weight' in x.keys():
                        key_name = 'weight'
                    x['qty']= str(float(x[key_name]) * float(new_quantity) * 1.0 / float(base_quantity))
                    new_state.append(x)
        
        new_output_state = []
        if len(state['quantities']) > 0:
            quantities = sorted([key for key in state['quantities'].keys()])
            base_quantity = quantities[0]
            new_quantity = quantity

            base_versions = sorted([version for version in state['quantities'][base_quantity].keys()])
            base_version = base_versions[1]

            if 'ingredient_app_output_state' in state['quantities'][base_quantity][base_version].keys():
                base_app_state_text = state['quantities'][base_quantity][base_version]['ingredient_app_output_state']
                base_app_state = json.loads(base_app_state_text)
                
                for x in base_app_state:
                    if 'qty' in x.keys():
                        key_name = 'qty'
                    if 'weight' in x.keys():
                        key_name = 'weight'
                    x['qty']= str(float(x[key_name]) * float(new_quantity) * 1.0 / float(base_quantity))
                    new_output_state.append(x)

        new_version_name = 'v'+str(latest_version)+' '+ datetime.datetime.today().strftime('%m/%d/%Y')

        if quantity not in state['quantities'].keys():
            state['quantities'][quantity]={}

        state['quantities'][quantity][new_version_name]={
            "recorder_app_data": "dummy",
            "ingredient_app_state": json.dumps(new_state),
            "ingredient_app_output_state": json.dumps(new_output_state),
            "converter_app_info": "dummy",
            "output_weight":quantity,
            "image": "dummy",
            "user_name": "",
        }
        
        state['quantities'][quantity]['latest_version']=latest_version
        sub_recipe['state']=json.dumps(state)

        query_manager.updateSubrecipes(subrecipe, sub_recipe)
        

    # ================================================
    # Else create new subrecipe
    # create quantity, then create version 1
    else:
        state = {
            "quantities" : {
                quantity : {
                    "latest_version": 1
                }
            }
        }
        latest_version = 1
        new_version_name = 'v'+str(latest_version)+' '+ datetime.datetime.today().strftime('%m/%d/%Y')
        new_state = []
        state['quantities'][quantity][new_version_name]={
            "recorder_app_data": "dummy",
            "ingredient_app_state": json.dumps(new_state),
            "ingredient_app_output_state": json.dumps(new_state),
            "converter_app_info": "dummy",
            "output_weight":"",
            "image": "dummy",
            "user_name": ""
        }
        state['quantities'][quantity]['latest_version']=latest_version
        state['shared_comment']=""

        data_to_send = [
                {
                    "subrecipeName" : subrecipe,
                    "state": json.dumps(state),
                }
            ]
        query_manager.addSubrecipes(data_to_send)


    response = JsonResponse({"message": "subrecipe created"})
    return mutate_response(response)

def convert_to_new_version(data):
    state = json.loads(data)

    for idx, x in enumerate(state):
        x['qty']=x['weight']
        x['srno']=idx + 1

    return json.dumps(state)

def get_saved_subrecipe(request):
    subrecipe=request.GET.get('recipe')
    quantity=str(request.GET.get('quantity'))
    version=request.GET.get('version')

    sub_recipe = query_manager.getSubrecipeName(subrecipe,state=True)

    state = json.loads(sub_recipe['state'])
    output_weight = state['quantities'][quantity][version]['output_weight']
    image = state['quantities'][quantity][version]['image']
    user_name = state['quantities'][quantity][version]['user_name']

    if 'shared_comment' not in state.keys():
        shared_comment = ""
    else:
        shared_comment = state['shared_comment']

    if 'ingredient_app_output_state' not in state['quantities'][quantity][version].keys():
        state['quantities'][quantity][version]['ingredient_app_output_state']= json.dumps([])

        old_state = state['quantities'][quantity][version]['ingredient_app_state']
        state['quantities'][quantity][version]['ingredient_app_state'] = convert_to_new_version(old_state)

    response = JsonResponse({
            "state_text": state['quantities'][quantity][version]['ingredient_app_state'],
            "output_state_text": state['quantities'][quantity][version]['ingredient_app_output_state'],
            "output_weight": output_weight,
            "image": image,
            "user_name": user_name,
            "shared_comment": shared_comment
        })

    return mutate_response(response)

@csrf_exempt
def save_subrecipe_state(request):
    subrecipe=urllib.parse.unquote(request.POST.get('recipe'))
    quantity=urllib.parse.unquote(request.POST.get('quantity'))
    version=urllib.parse.unquote(request.POST.get('version'))
    image=urllib.parse.unquote(request.POST.get('image'))
    list_state=urllib.parse.unquote(request.POST.get('state'))
    output_list_state=urllib.parse.unquote(request.POST.get('output_state'))
    output_weight=urllib.parse.unquote(request.POST.get('output_weight'))
    user_name=urllib.parse.unquote(request.POST.get('user_name'))
    shared_comment=urllib.parse.unquote(request.POST.get('shared_comment'))


    sub_recipe = query_manager.getSubrecipeName(subrecipe,state=True)
    state = json.loads(sub_recipe['state'])
    state['quantities'][quantity][version]['ingredient_app_state']=list_state
    state['quantities'][quantity][version]['ingredient_app_output_state']=output_list_state
    state['quantities'][quantity][version]['output_weight']=output_weight
    state['quantities'][quantity][version]['image']=image
    state['quantities'][quantity][version]['user_name']=user_name
    state['shared_comment']=shared_comment
    sub_recipe['state']=json.dumps(state)
    
    query_manager.updateSubrecipes(subrecipe, sub_recipe)
    return JsonResponse({"message":"saved"})
 
def get_quantities(request):
    recipe_name = request.GET.get('recipe_name',None)

    # recipe_name = oi_main_cook_map[recipe_name]
    sub_recipe = query_manager.getSubrecipeName(recipe_name,state=True)
    state = json.loads(sub_recipe['state'])
    quantities = [x for x in state['quantities'].keys()]

    # quantities = [100,200]
    response = JsonResponse({"quantities": quantities})
    return mutate_response(response)

def get_versions(request):
    recipe_name = request.GET.get('recipe_name',None)
    quantity = int(request.GET.get('quantity',None))

    print(recipe_name)
    print(quantity)

    # recipe_name = oi_main_cook_map[recipe_name]
    sub_recipe = query_manager.getSubrecipeName(recipe_name, state=True)
    state = json.loads(sub_recipe['state'])

    versions = [x for x in state['quantities'][str(quantity)].keys()]
    versions.remove('latest_version')
    # versions = ['v1','v2']

    response = JsonResponse({"versions": versions})
    return mutate_response(response)

def save_logs(request):
    subrecipe=urllib.parse.unquote(request.GET.get('recipe'))
    quantity=urllib.parse.unquote(request.GET.get('quantity'))
    version=urllib.parse.unquote(request.GET.get('version'))
    logs=urllib.parse.unquote(request.GET.get('logs'))

    sub_recipe = query_manager.getSubrecipeName(subrecipe,state=True)

    state = json.loads(sub_recipe['state'])
    state['quantities'][quantity][version]['recorder_app_data']=logs
    sub_recipe['state']=json.dumps(state)

    query_manager.updateSubrecipes(subrecipe, sub_recipe)
    return JsonResponse({"message":"logs saved"})

@csrf_exempt
def save_image(request):
    subrecipe=urllib.parse.unquote(request.POST.get('recipe'))
    quantity=urllib.parse.unquote(request.POST.get('quantity'))
    version=urllib.parse.unquote(request.POST.get('version'))
    image=urllib.parse.unquote(request.POST.get('image'))

    sub_recipe = query_manager.getSubrecipeName(subrecipe,state=True)

    state = json.loads(sub_recipe['state'])
    state['quantities'][quantity][version]['image']=image
    sub_recipe['state']=json.dumps(state)

    query_manager.updateSubrecipes(subrecipe, sub_recipe)
    return JsonResponse({"message":"image saved"})