from python_graphql_client import GraphqlClient
import os
# import multiprocessing as mp


class GqlHelper():
    def __init__(self):
        super(GqlHelper, self).__init__()
        self.client = GraphqlClient(
            # endpoint="http://192.168.0.112:3000/graphql")
            # endpoint="http://cloud-chef-kitchen-manager_backend_1:3000/graphql")
            endpoint=os.getenv('DATABASE_URL',"http://backend:3000/graphql"))
        self.mutationAddIngredients = """
            mutation AddIngredients($records: [CreateManyIngredientInput!]!) {
                ingredientCreateMany(records: $records) {
                    records{
                        displayName
                    }
                }
            }
        """

        self.mutationAddOrderableItems = """
            mutation AddOrderableItems($records: [CreateManyOrderableItemInput!]!) {
                orderableitemCreateMany(records: $records) {
                    records{
                        mainCook
                    }
                }
            }
        """

        self.mutationAddBrands = """
            mutation AddBrands($records: [CreateManyBrandInput!]!) {
                brandCreateMany(records: $records) {
                    records {
                        name
                    }
                }
            }
        """

        self.mutationUpdateIngredients = """
            mutation UpdateIngredients($id: MongoID!, $record: UpdateByIdIngredientInput!) {
                ingredientUpdateById(_id: $id, record: $record) {
                    record {
                        displayName
                        isStorable
                    }
                }
            }
        """

        self.queryIngredientName = """
            query getIngredient($ingredientName: String!) {
                ingredientOne(filter:{ingredientName: $ingredientName}){
                    _id
                    displayName
                    ingredientName
                    allowedPortions
                    allowedContainers {
                        unit
                        containers
                    }
                    isStorable
                    unitOfMeasurement
                    recipes
                    scalableRecipe
                    recipeLinks
                }
            }
        """

        self.queryOrderableItemName = """
            query getOrderableItem($orderableItemName: String!) {
                orderableitemOne(filter:{orderableItemName: $orderableItemName}) {
                    _id
                    mainCook
                    accompaniments
                    brand
                    orderableItemName
                }
            }
        """

        self.queryBrands = """
            query{
                brandMany{
                    name
                }
            }
        """

        self.queryOrderableItemByBrand = """
        query getOrderableItemBrand($brandName: String!){
            orderableitemMany( filter:{
                brand: $brandName
            }){
                orderableItemName
            }
        }
        """

        self.queryOrderableItemAll = """
        query{
            orderableitemMany{
                orderableItemName
            }
        }
        """

        self.queryIngredientAll= """
            query getIngredientAll{
                ingredientMany{
                    _id
                    displayName
                    ingredientName
                    allowedPortions
                    allowedContainers {
                        unit
                        containers
                    }
                    isStorable
                    unitOfMeasurement
                    recipes
                    scalableRecipe
                    recipeLinks
                }
            }
        """

        self.querySubrecipeName = """
            query getSubRecipe($subrecipeName: String!) {
                subrecipeOne(filter:{subrecipeName: $subrecipeName}) {
                    ingredients
                    subrecipeName
                    _id
                    # state
                }
            }
        """

        self.querySubrecipeNameState = """
            query getSubRecipe($subrecipeName: String!) {
                subrecipeOne(filter:{subrecipeName: $subrecipeName}) {
                    ingredients
                    subrecipeName
                    _id
                    state
                }
            }
        """

        self.querySubrecipeAll = """
            query getSubRecipe{
                subrecipeMany{
                    ingredients
                    subrecipeName
                    _id
                    # state
                }
            }
        """

        self.querySubrecipeAllState = """
            query getSubRecipe{
                subrecipeMany{
                    ingredients
                    subrecipeName
                    _id
                    state
                }
            }
        """
        
        self.mutationUpdateSubrecipes = """
            mutation UpdateSubrecipes($id: MongoID!, $record: UpdateByIdSubrecipeInput!) {
                subrecipeUpdateById(_id: $id, record: $record) {
                    record {
                        ingredients
                        subrecipeName
                    }
                }
            }
        """

        self.mutationAddSubrecipes = '''
            mutation AddSubrecipes($records: [CreateManySubrecipeInput!]!) {
                subrecipeCreateMany(records: $records) {
                    records {
                        subrecipeName
                    }
                }
            }
        '''

    def updateIngredientByIngredientName(self, name: str, payload={}):
        variables = {"ingredientName": name}
        id = self.client.execute(
            query=self.queryIngredientName, variables=variables)['data']['ingredientOne']['_id']
        variables = {
            "id": id,
            "record": payload
        }
        self.client.execute(
            query=self.mutationUpdateIngredients, variables=variables)['data']
    #logging.debug('%s updated', name)

    def getOrderableItem(self, orderableItemName):
        variables = {"orderableItemName": orderableItemName}
        obj = self.client.execute(query=self.queryOrderableItemName, variables=variables)[
            'data']['orderableitemOne']
        #print(obj)
        return obj

    def addMultipleIngredients(self, ingredients):
        variables = {"records": ingredients}
        queryIngredientNameData = self.client.execute(
            query=self.mutationAddIngredients, variables=variables)['data']
        #print(queryIngredientNameData)
        # logging.debug(queryIngredientNameData)

    def addMultipleOrderableItems(self, orderableItems):
        variables = {"records": orderableItems}
        queryOrderableItemsData = self.client.execute(
            query=self.mutationAddOrderableItems, variables=variables)['data']
        #print(queryOrderableItemsData)

    def addMultipleBrands(self, brands):
        variables = {"records": brands}
        queryBrandsData = self.client.execute(
            query=self.mutationAddBrands, variables=variables)['data']
        #print(queryBrandsData)

    # Get all brands
    def getMultipleBrands(self):
        brands = self.client.execute(
            query=self.queryBrands)['data']
        return brands['brandMany']

    # Get recipes supported by brand
    def getOrderableItemsByBrand(self, brand=None):
        if brand:
            variables = {"brandName": brand}
            queryOrderableItemsData = self.client.execute(
                query=self.queryOrderableItemByBrand, variables=variables)['data']
        else:
            variables = {"brandName": brand}
            queryOrderableItemsData = self.client.execute(
                query=self.queryOrderableItemAll, variables=variables)['data']

        return queryOrderableItemsData['orderableitemMany']

    # Get all ingredients
    def getIngredientAll(self):
        ingredient = self.client.execute(
            query=self.queryIngredientAll)['data']
        return ingredient['ingredientMany']

    def getIngredient(self, ingredientName):
        variables = {"ingredientName": ingredientName}
        ingr = self.client.execute(
            query=self.queryIngredientName, variables=variables)['data']['ingredientOne']
        #print(ingr)
        return ingr

    # TODO CHECK
    def getSubrecipeName(self, subrecipeName, state=False):
        if not state:
            variables = {"subrecipeName": subrecipeName}
            ingr = self.client.execute(
                query=self.querySubrecipeName, variables=variables)
            ingr = ingr['data']['subrecipeOne']
        else:
            variables = {"subrecipeName": subrecipeName}
            ingr = self.client.execute(
                query=self.querySubrecipeNameState, variables=variables)
            ingr = ingr['data']['subrecipeOne']
        return ingr

    def getSubrecipeAll(self, state=False):
        if state:
            ingr = self.client.execute(
                query=self.querySubrecipeAllState)
            ingr = ingr['data']['subrecipeMany']
        else:    
            ingr = self.client.execute(
                query=self.querySubrecipeAll)
            ingr = ingr['data']['subrecipeMany']
        return ingr

    # TODO  CHECK
    def addSubrecipes(self, data):
        variables = {"records": data}
        subrecipeData = self.client.execute(
            query=self.mutationAddSubrecipes, variables=variables)['data']  
    
    # TODO 
    def updateSubrecipes(self, name, data):
        variables = {"subrecipeName": name}
        ingr = self.client.execute(
            query=self.querySubrecipeName, variables=variables)
        ingr = ingr['data']['subrecipeOne']
        id = ingr['_id']

        data.pop('_id')
        variables = {
            "id": id,
            "record": data
        }

        self.client.execute(
            query=self.mutationUpdateSubrecipes, variables=variables)['data']


# state = {
#     "quantities" : {
#         100 : {
#             "latest_version":3,
#             "v1" : {
#                 "recorder_app_data_name": "zzzzz",
#                 "ingredient_app_state": "state",
#                 "ingredient_app_state": "state",
#                 "output_weight": "output_weight",
#             },
#             "v2" : {

#             },
#             "v3" : {

#             },
#         },        
#         200 : {
#             "v1" : {}
#         }
#     }
# }