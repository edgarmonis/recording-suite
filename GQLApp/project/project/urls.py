"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from ingredients import views



from django.contrib.staticfiles.urls import staticfiles_urlpatterns


router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('search', views.search_ingredient, name='search'),
    path('get_brands', views.get_brands, name='get_brands'),
    path('get_recipes', views.get_recipes, name='get_recipes'),
    path('get_subrecipes', views.get_subrecipes, name='get_subrecipes'),
    path('get_ingredients', views.get_ingredients, name='get_ingredients'),
    path('get_quantities', views.get_quantities, name='get_quantities'),
    path('get_versions', views.get_versions, name='get_versions'),
    path('create_subrecipe', views.create_subrecipes, name='create_subrecipes'),
    path('get_saved_subrecipe', views.get_saved_subrecipe, name='get_saved_subrecipe'),
    path('save_subrecipe_state', views.save_subrecipe_state, name='save_subrecipe_state'),
    path('save_logs', views.save_logs, name='save_logs'),
    path('save_image', views.save_image, name='save_image'),
    path('init', views.init, name='init'),
]

urlpatterns += staticfiles_urlpatterns()
