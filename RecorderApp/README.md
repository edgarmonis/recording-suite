# Recording-webapp

A webapp for recording recipes

## Test and Deploy
```
docker-compose up --build
```
Please ensure that port 8000 is free, if not, changes need to be made to

docker-compose.yaml
/project/daemon.sh

## Name
Recipe Recorder WebApp


## Description
A Django + jquery + bootstrap webapp for creation of logs, sending and recieving mqtt control messages to external devices and creation of a 
download.zip which contains all the generated data

## Usage
Go to http://0.0.0.0:8000/home to reach the recorder landing page

After entering data you will be redirected to one of
http://0.0.0.0:8000/main/induction
http://0.0.0.0:8000/main/blender-controlled
http://0.0.0.0:8000/main/blender-manual

## Architecture
[link](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/Architecture.md)

## Support
MAINTAINER
edgarmonis@cloudchef.co

## Roadmap
1) Add more devices

## Authors and acknowledgment
google, stackoverflow and the cool kids at cloudchef :)

## License
Closed Source

## Project status
In Development

## For dev
apt-get install mosquitto
apt-get install systemd
service  mosquitto stop
service mosquitto start 

see mqtt message 
service  mosquitto stop
mosquitto -v

