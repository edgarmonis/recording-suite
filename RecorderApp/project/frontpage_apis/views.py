import random
from django.contrib.auth.models import User, Group
from django.shortcuts import render

from rest_framework import viewsets
from rest_framework import permissions
from frontpage_apis.serializers import RecordsSerializer

from frontpage_apis.models import Records

class RecordsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Records.objects.all()
    serializer_class = RecordsSerializer
    lookup_field = 'id' 
    # permission_classes = [permissions.IsAuthenticated]

def index(request):
    """View function for home page of site."""

    uid = random.randint(0, 100000000)
    context = {
        "uid":uid
    }
    
    response = render(request, 'index.html', context=context)
    response.set_cookie('uid', uid)
    # Render the HTML template index.html with the data in the context variable
    return response

def get_selector(request):
    return render(request, "selector.html")