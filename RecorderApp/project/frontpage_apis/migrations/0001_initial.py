# Generated by Django 3.2.6 on 2021-08-06 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Records',
            fields=[
                ('uid', models.IntegerField(primary_key=True, serialize=False)),
                ('record_name', models.TextField()),
                ('record_type', models.TextField(choices=[('', ''), ('IN', 'Induction'), ('BL', 'Blender'), ('OF', 'Off-pan')], default='')),
                ('fps_equipment', models.TextField(blank=True, choices=[('', ''), ('FPS1', 'Food Processing Station 1'), ('FPS2', 'Food Processing Station 2'), ('FPS3', 'Food Processing Station 3')], default='')),
                ('blender_equipment', models.TextField(blank=True, choices=[('', ''), ('Blender1', 'Blender 1'), ('Blender2', 'Blender 2'), ('Blender3', 'Blender 3')], default='')),
                ('record_mode', models.TextField(blank=True, choices=[('', ''), ('Manual', 'Manual'), ('Controlled', 'Controlled')], default='')),
            ],
        ),
    ]
