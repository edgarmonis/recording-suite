from django.contrib.auth.models import User, Group
from rest_framework import serializers

from frontpage_apis.models import Records

class RecordsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Records
        fields = ['uid', 'record_name','record_type', 'fps_equipment', 'fps_equipment_bulk', 'blender_equipment', 'record_mode']