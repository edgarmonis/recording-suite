from django.db import models

from frontpage_apis.choices import (
    RecordTypeChoices, FPSTypeChoices, FPSTypeChoicesBulk, BlenderTypeChoices,
    BlenderRecordModeTypeChoices)

# Create your models here.
class Records(models.Model):
    uid = models.IntegerField(primary_key=True)

    record_name = models.TextField()

    record_type = models.TextField(
        choices=RecordTypeChoices.choices,
        default=RecordTypeChoices.DEFAULT
    )
    
    fps_equipment = models.TextField(
        choices=FPSTypeChoices.choices,
        blank=True,
        default=''
    )

    fps_equipment_bulk = models.TextField(
        choices=FPSTypeChoicesBulk.choices,
        blank=True,
        default=''
    )

    blender_equipment = models.TextField(
        choices=BlenderTypeChoices.choices,
        blank=True,
        default=''
    )
    record_mode = models.TextField(
        choices=BlenderRecordModeTypeChoices.choices,
        blank=True,
        default=''
    )