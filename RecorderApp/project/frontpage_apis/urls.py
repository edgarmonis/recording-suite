from django.contrib import admin
from django.urls import include, path

from frontpage_apis import views

urlpatterns = [
    path('', views.index, name='index'),
]