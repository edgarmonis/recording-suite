from django.db import models

class RecordTypeChoices(models.TextChoices):
    DEFAULT = '',''
    INDUCTION = 'IN', 'Induction'
    INDUCTIONBULK = 'INB', 'Bulk Induction'
    BLENDER = 'BL', 'Blender'
    OFFPAN = 'OF', 'Off-pan'

class FPSTypeChoices(models.TextChoices):
    DEFAULT = '',''
    FPS1 = 'FPS1', 'Food Processing Station 1'
    FPS2 = 'FPS2', 'Food Processing Station 2'
    FPS3 = 'FPS3', 'Food Processing Station 3'
    FPS4 = 'FPS4', 'Food Processing Station 4'
    FPS5 = 'FPS5', 'Food Processing Station 5'

class FPSTypeChoicesBulk(models.TextChoices):
    DEFAULT = '',''
    FPS1 = 'FPS9', 'Food Processing Station 9'

class BlenderTypeChoices(models.TextChoices):
    DEFAULT = '',''
    BLENDER1 = 'Blender1', 'Blender 1'
    BLENDER2 = 'Blender2', 'Blender 2'
    BLENDER3 = 'Blender3', 'Blender 3'

class BlenderRecordModeTypeChoices(models.TextChoices):
    DEFAULT = '',''
    MANUAL = 'Manual', 'Manual'
    CONTROLLED = 'Controlled', 'Controlled'