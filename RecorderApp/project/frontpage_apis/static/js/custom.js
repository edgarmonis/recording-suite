$( "#contact_form" ).submit(function(e) {
  e.preventDefault();
  var dataString = $(this).serializeArray();

  console.log(dataString)

  data = {
    "uid": parseInt(dataString[0]["value"]),
    "record_name":dataString[1]["value"],
    "record_type":dataString[2]["value"],
    "fps_equipment":dataString[3]["value"],
    "fps_equipment_bulk":dataString[4]["value"],
    "blender_equipment":dataString[5]["value"],
    "record_mode":dataString[6]["value"],
  }
  
  if(data["record_name"]==""){
    document.getElementById("recipe_name").placeholder = "Please add a recipe Name";
  }

  if(data["record_type"]=="IN"){
    data["blender_equipment"] = "";
    data["record_mode"] = "";
    data["fps_equipment_bulk"] = "";
  }
  if(data["record_type"]=="INB"){
    data["blender_equipment"] = "";
    data["record_mode"] = "";
    data["fps_equipment"] = "";
  }
  if(data["record_type"]=="BL"){
    data["fps_equipment"] = "";
    data["fps_equipment_bulk"] = "";
  }

  data["record_name"] = data["record_name"].replaceAll(" ", "_")
  setCookie("uid",data["uid"],20)
  setCookie("record_name",data["record_name"],20)
  setCookie("record_type",data["record_type"],20)
  setCookie("fps_equipment",data["fps_equipment"],20)
  setCookie("fps_equipment_bulk",data["fps_equipment_bulk"],20)
  setCookie("blender_equipment",data["blender_equipment"],20)
  setCookie("record_mode",data["record_mode"],20)

  console.log(data)

  var equipment;
  // console.log(data)
  if(data["record_type"]=="BL"){
    // Send MQTT Message
    equipment = data["blender_equipment"]
  }
  else if(data["record_type"]=="INB") {
    // send bulk induction message
    equipment = data["fps_equipment_bulk"]
  }
  else{
    // send induction message
    equipment = data["fps_equipment"]
  }
  setCookie("equipment",equipment,20)

  console.log("Equipment is " + equipment)

  $.ajax({
    type: "GET",
    url: "/main/init",
    data: {
      "equipment":equipment
    },
    async : false,
    success: function (response) {
      console.log("rpi init hit success")
    },
    error: function (response) {
      // alert the error if any error occured
      console.log("rpi init failed");
    }
  });

  if(data['record_type']=="IN"){
    location.href = "/main/induction";   
  }
  if(data['record_type']=="INB"){
    location.href = "/main/bulk_induction";   
  }
  if(data['record_type']=="BL"){
    if(data['record_mode']=="Manual"){
      location.href = "/main/blender-manual";  
    }
    else{
      location.href = "/main/blender-controlled";  
    }
  }
  
});

$( "#RecordingSelect" ).change(function(e) {
  e.preventDefault();
  var recordingType = document.getElementById('RecordingSelect');
  console.log(recordingType.value)

  if (recordingType.value == "IN"){
    // Make visible
    document.getElementById("RecordingEquipmentSelect").classList.remove('invisible');
    
    // Make invisible
    document.getElementById("RecordingBlenderEquipmentSelect").classList.add('invisible');  
    document.getElementById("BlenderRecordingTypeSelect").classList.add('invisible'); 
    document.getElementById("BulkRecordingEquipmentSelect").classList.add('invisible'); 
  }
  if (recordingType.value == "INB"){
    // Make visible
    document.getElementById("BulkRecordingEquipmentSelect").classList.remove('invisible');
    
    // Make invisible
    document.getElementById("RecordingBlenderEquipmentSelect").classList.add('invisible');  
    document.getElementById("BlenderRecordingTypeSelect").classList.add('invisible');  
    document.getElementById("RecordingEquipmentSelect").classList.add('invisible');
  }
  if (recordingType.value == "BL"){
    // Make visible
    document.getElementById("RecordingBlenderEquipmentSelect").classList.remove('invisible');
    document.getElementById("BlenderRecordingTypeSelect").classList.remove('invisible');

    // Make invisible
    document.getElementById("RecordingEquipmentSelect").classList.add('invisible');  
    document.getElementById("BulkRecordingEquipmentSelect").classList.add('invisible');
  }
  if (recordingType.value == "OF"){
    // Make visible
    document.getElementById("RecordingBlenderEquipmentSelect").classList.add('invisible');  
    document.getElementById("BlenderRecordingTypeSelect").classList.add('invisible');

    // Make invisible
    document.getElementById("RecordingEquipmentSelect").classList.add('invisible');  
    document.getElementById("BulkRecordingEquipmentSelect").classList.add('invisible');
  }
});

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};