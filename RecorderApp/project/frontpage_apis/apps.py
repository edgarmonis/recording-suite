from django.apps import AppConfig


class FrontpageApisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'frontpage_apis'
