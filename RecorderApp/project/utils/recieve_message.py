import paho.mqtt.client as paho
import json
from multiprocessing.connection import Client

import threading
import subprocess
import time

from utils.log_and_map import logger
from utils.message_handler import handle_hardware_error, handle_induction_level, handle_setLevel, handle_temp_read, handle_weight_read

import random

class ServerClient():
	def __init__(self):
		self.password=b"ch6gtfg12#"
	
	def subscribe(self, equipment):
		address = ('localhost', 6000)
		conn = Client(address, authkey=self.password)
		
		conn.send(['sub',equipment])
		conn.close()

	def unsubscribe(self, equipment):
		address = ('localhost', 6000)
		conn = Client(address, authkey=self.password)
		conn.send(['unsub',equipment])
		conn.close()