import json
from mainrecorder.models import State
from utils.log_and_map import logger

def handle_setLevel(msg):
    equipment = msg['senderId']

    obj, created = State.objects.update_or_create(
        uid=equipment,
        defaults={
            'blender_level':msg['payload'],
        }
    )
    logger.info("blender level of "+ equipment + " updated")

def handle_hardware_error(msg):
    equipment = msg['senderId']

    obj, created = State.objects.update_or_create(
        uid=equipment,
        defaults={
            'hardware_error':msg['payload'],
        }
    )
    logger.info("hardware error of "+ equipment + " updated")

def handle_induction_level(msg):
    equipment = msg['senderId']

    obj, created = State.objects.update_or_create(
        uid=equipment,
        defaults={
            'induction_level':msg['payload'],
        }
    )
    logger.info("induction level of "+ equipment + " updated")

def handle_weight_read(msg):
    equipment = msg['senderId']

    obj, created = State.objects.update_or_create(
        uid=equipment,
        defaults={
            'weight':msg['payload'],
        }
    )
    logger.info("weight of "+ equipment + " updated")

def handle_temp_read(msg):
    equipment = msg['senderId']

    obj, created = State.objects.update_or_create(
        uid=equipment,
        defaults={
            'temp':msg['payload'],
        }
    )
    logger.info("temp of "+ equipment + " updated")