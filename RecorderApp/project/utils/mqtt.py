import multiprocessing as mp
import paho.mqtt.client as paho

import json
import os
import time
import datetime
from utils.log_and_map import logger
from multiprocessing.connection import Client

# receiveQ = mp.Queue()
# transmitQ = mp.Queue()

class NetworkMessage():
    def __init__(self, receiverId, senderId="Rec1", function=None, payload=None, taskId=None):
        self.taskId = taskId
        self.function = function
        self.receiverId = receiverId
        self.senderId = senderId
        self.payload = payload
    
	# Helper message for getting channel
    def getChannel(self):
        channel = self.senderId + "/" + self.receiverId    
        return channel

    @classmethod
    def fromJSON(cls, jsonObj):
        return cls(jsonObj["receiverId"], jsonObj["senderId"], jsonObj["function"], jsonObj["payload"], jsonObj["taskId"])

    def getMessage(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)

    def printMessage(self):
        print(self.taskId, self.function, self.receiverId, self.senderId, self.payload)


# class MqttClient(mp.Process):
# 	def __init__(self, broker, kcList):
# 		super().__init__()
# 		# self.transmitQ = transmitQ
# 		# self.receiveQ = receiveQ
# 		self.broker = broker
# 		self.kcList = kcList

# 	def on_message(self, client, userdata, message):
# 		rMsg = json.loads(message.payload)
# 		nmObj = NetworkMessage.fromJSON(rMsg)
# 		print("got mqtt")
# 		self.receiveQ.put(nmObj)

# 	def run(self):
# 		self.client = paho.Client("Recording_"+str(time.time()))
# 		self.client.on_message = self.on_message
# 		self.client.connect(self.broker, 1883, keepalive=60)
# 		print("connected")
# 		for kc in self.kcList:
# 			print(kc)
# 			self.client.subscribe(kc + "/Rec1")

# 		self.client.loop_start()
# 		self.processTransmitQ()

	# This is the main function reponsible for sending data over MQTT,
	# It adds messages (NMessage) to a queue which transmits data 
	# untill empty
	# def processTransmitQ(self):
	# 	while True:
	# 		while not self.transmitQ.empty():
	# 			logger.info("transmitting message")
	# 			transmitInstr = self.transmitQ.get()
	# 			self.client.publish(transmitInstr.getChannel(), transmitInstr.getMessage(), qos=2)
	# 			logger.info("message published")



def send_mqtt(uid, equipment, function, payload):	
	address = ('localhost', 6000)
	conn = Client(address, authkey=b"ch6gtfg12#")
	
	conn.send(['pub',equipment, function, payload])
	conn.close()

	# mqttClient = paho.Client("Recording_"+str(time.time()), clean_session=True)
	# broker = os.getenv("BROKER")
	# mqttClient.connect(broker, 1883, keepalive=60)

	# Convert DB entry to deafult standard
	# equipment = equipment.lower()
	# nMessage = NetworkMessage(receiverId = equipment, function = function)
	
	# # Parse numbers for numeric payloads
	# if function == "setLevel" or function == "inductionLevel":
	# 	payload = int(payload)

	# # Assign payload
	# nMessage.payload = payload
	# rc = mqttClient.publish(nMessage.getChannel(), nMessage.getMessage(), qos=2)
	
	# logger.info(nMessage.getChannel())

	# # rc.wait_for_publish()
	# logger.info("MQTT Message published")
	# mqttClient.disconnect()


# import paho.mqtt.client as paho

# broker='192.168.0.112'
    
# mqttClient = paho.Client("TESTTSTSTTS", clean_session=True)
# mqttClient.connect(broker, 1883, keepalive=60)
# rc = mqttClient.publish('Rec1/fps2','Hello from the other side', qos=2)

# import pdb
# pdb.set_trace()