import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("gunicorn.info")

fpsIPMap = {
    'fps1': '192.168.0.55',
    'fps2': '192.168.0.79',
    'fps3': '192.168.0.70',
    'fps4': '192.168.0.82',
    'fps5': '192.168.0.80',
    'fps9': '192.168.0.90',
    'blender1': '192.168.0.55',
    'blender2': '192.168.0.79',
    'blender3': '192.168.0.70',
    'blender4': '192.168.0.82',
}