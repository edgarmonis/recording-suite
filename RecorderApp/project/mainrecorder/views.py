import json
import subprocess
import pickle

from django.contrib.auth.models import User, Group
from django.core import serializers
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from rest_framework import viewsets
from rest_framework import permissions

from mainrecorder.serializers import LogSerializer, StateSerializer#, TempSerializer, WeightSerializer, BlenderLevelSerializer, HardwareErrorSerializer, InductionSerializer
from mainrecorder.models import LogFile, State#, TempFile, WeightFile, BlenderLevelFile, HardwareErrorFile, Induction

from utils.mqtt import send_mqtt
from utils.log_and_map import fpsIPMap, logger
from utils.recieve_message import ServerClient
from utils.message_handler import handle_hardware_error, handle_induction_level, handle_setLevel, handle_temp_read, handle_weight_read

CLIENT = ServerClient()

class LogViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = LogFile.objects.all()
    serializer_class = LogSerializer

class StateViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = State.objects.all()
    serializer_class = StateSerializer

def run_init(request):    
    # Start RPI script
    equipment=request.COOKIES['equipment'].lower()
    logger.info('Here is the Init script for ' + equipment)
    logger.info('sshpass -p \'CloudChef12#\' ssh pi@'+ fpsIPMap[equipment] +' "cd Desktop/FPS/ && python3 fpsRecording.py"')
    State.objects.update_or_create(
            uid=equipment,
            defaults={
                    'uid': equipment,
                    'blender_level': '0', 
                    'hardware_error': False, 
                    'induction_level': '0', 
                    'weight': '0', 
                    'temp': '0'
                }
        )
    # Start Client
    # subprocess.call('sshpass -p CloudChef12# ssh pi@'+ fpsIPMap[equipment] +' "cd Desktop/FPS/ && python3 fpsRecording.py"', shell = True)
    uid = request.COOKIES['uid']

    logger.info('setting client '+ str(uid))
    CLIENT.subscribe(equipment+"/Rec1")
    return JsonResponse({"success":200})

def get_log(request):
    uid = request.GET['uid']
    log = LogFile.objects.filter(uid=uid)

    qs_json = json.loads(serializers.serialize('json', log))
    log_text = qs_json[0]['fields']['log']

    data = {
        "log": log_text
    }

    return JsonResponse(data)

def get_state(request):
    equipment = request.COOKIES['equipment'].lower()

    obj = State.objects.filter(uid=equipment)

    qs_json = json.loads(serializers.serialize('json', obj))
    data =  qs_json[0]['fields']

    return JsonResponse(data)



def get_temp(request):
    equipment = request.COOKIES['equipment'].lower()
    temp_text = temp_map[equipment]

    logger.info(str(temp_text) + " is the temp")
    data = {
        "temp": temp_text
    }
    return JsonResponse(data)

def get_weight(request):
    equipment = request.COOKIES['equipment'].lower()
    temp_text = weight_map[equipment]

    logger.info(str(temp_text) + " is the wt")

    data = {
        "weight": temp_text
    }
    return JsonResponse(data)

def get_error_level(request):
    equipment = request.COOKIES['equipment'].lower()
    temp_text = hardware_error_map[equipment]

    logger.info(str(temp_text) + " is the error")

    if hardware_error_map[equipment]:
        hardware_error_map[equipment]=False

    data = {
        "error": temp_text
    }
    return JsonResponse(data)

def get_blender_level(request):
    equipment = request.COOKIES['equipment'].lower()
    temp_text = blender_level_map[equipment]

    logger.info(str(temp_text) + " is the bl level")

    data = {
        "bl_level": temp_text
    }
    return JsonResponse(data)


def induction(request):
    """View function for home page of site."""
    context = {}
    uid = request.COOKIES['uid']
    return render(request, 'induction.html', context=context)

def bulk_induction(request):
    """View function for home page of site."""
    context = {}
    uid = request.COOKIES['uid']
    return render(request, 'bulk_induction.html', context=context)

def blender_manual(request):
    """View function for home page of site."""
    context = {}
    uid = request.COOKIES['uid']
    return render(request, 'blender_manual.html', context=context)

def blender_controlled(request):
    """View function for home page of site."""
    context = {}
    uid = request.COOKIES['uid']
    return render(request, 'blender_controlled.html', context=context)

def mqtt(request):
    uid=request.COOKIES['uid']
    equipment=request.GET['equipment']
    function = request.GET['function']
    payload = request.GET['payload']
    send_mqtt(uid, equipment, function, payload)
    return JsonResponse({'foo':'bar'})

def on_mqtt_message(request):
    recvMsg = request.GET['msg']
    recvMsg = json.loads(recvMsg)
    logger.info("channel message recieved")
    logger.info(recvMsg)

    if recvMsg['function']=="setLevel":
        handle_setLevel(recvMsg)

    elif recvMsg['function']=="hardwareError":
        handle_hardware_error(recvMsg)

    elif recvMsg['function']=="inductionLevel":
        handle_induction_level(recvMsg)

    elif recvMsg['function']=="weightRead":
        handle_weight_read(recvMsg)

    elif recvMsg['function']=="temperatureRead":
        handle_temp_read(recvMsg)

    return JsonResponse({"message":"success"})

if __name__ == "__main__":
    import pdb
    pdb.set_trace()