from django.apps import AppConfig


class MainrecorderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mainrecorder'
