from django.contrib import admin
from django.urls import include, path
import download.views as downloadView

from mainrecorder import views

urlpatterns = [
    path('induction', views.induction, name='induction'),
    path('bulk_induction', views.bulk_induction, name='induction'),
    path('blender-manual', views.blender_manual, name='blender_manual'),
    path('blender-controlled', views.blender_controlled, name='blender_controlled'),
    path('send-mqtt', views.mqtt, name='send_mqtt'),
    path('get-log', views.get_log, name='get_log'),
    path('get-temp', views.get_temp, name='get_temp'),
    path('get-state', views.get_state, name='get_state'),
    path('get-weight', views.get_weight, name='get_weight'),
    path('get-bl-level', views.get_blender_level, name='get_bl_level'),
    path('get-error', views.get_error_level, name='get_error'),
    path('on-mqtt-message', views.on_mqtt_message, name='mqtt_message'),
    path('download', downloadView.get_download, name='download'),
    path('get-zip', downloadView.get_zip, name='get_zip'),
    path('init', views.run_init, name='rpi_init'),
]