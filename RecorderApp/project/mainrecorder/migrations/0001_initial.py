# Generated by Django 3.2.6 on 2021-08-06 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Induction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('induction_level', models.TextField(choices=[('0', '0'), ('300', '1'), ('500', '2'), ('800', '3'), ('1000', '4'), ('1200', '5'), ('1600', '6'), ('2000', '7'), ('2500', '8')], default='0')),
            ],
        ),
        migrations.CreateModel(
            name='LogFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.IntegerField()),
                ('log', models.TextField()),
            ],
        ),
    ]
