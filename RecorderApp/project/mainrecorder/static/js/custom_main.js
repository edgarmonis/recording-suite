$( "#induction_button" ).click(function(e) {
  e.preventDefault();

  inductionPowerMap = {
		'0': 0,
		'1': 300,
		'2': 500,
		'3': 800,
		'4': 1000,
		'5': 1200,
		'6': 1600,
		'7': 2000,
		'8': 2500
	}
  
  // call mqtt message


  // Update logs
  console.log("induction message sent")

  var d = new Date();
  var curr_time = d.getTime();
  
  timestamp = Math.round((curr_time - getCookieValue("start_time") )/ 1000)
  induction_level = document.getElementById("Induction").value  
  wattage = inductionPowerMap[induction_level]

  document.getElementById("Logs").value += timestamp 
  + ", Induction level: " + induction_level + " (" + wattage+")"+"\n"


  // Update UI
  $("#IL_Text").html(induction_level);
  send_mqtt(getCookieValue("fps_equipment"),"inductionLevel",parseInt(induction_level))
  
});

$( "#induction_button_bulk" ).click(function(e) {
  e.preventDefault();

  inductionPowerMap = {
		'0': 0,
		'1': 817,
		'2': 1687,
		'3': 2575,
		'4': 3518,
		'5': 4460,
		'6': 5333,
		'7': 6162,
		'8': 7500
	}
  
  // call mqtt message


  // Update logs
  console.log("induction message sent")

  var d = new Date();
  var curr_time = d.getTime();
  
  timestamp = Math.round((curr_time - getCookieValue("start_time") )/ 1000)
  induction_level = document.getElementById("Induction").value  
  wattage = inductionPowerMap[induction_level]

  document.getElementById("Logs").value += timestamp 
  + ", Induction level: " + induction_level + " (" + wattage+")"+"\n"


  // Update UI
  $("#IL_Text").html(induction_level);
  send_mqtt(getCookieValue("fps_equipment_bulk"),"inductionLevel",parseInt(induction_level))
  
});

$( "#Blender" ).change(function(e) {
  e.preventDefault();  
  // call mqtt message


  // Update logs
  console.log("blender message sent")

  var d = new Date();
  var curr_time = d.getTime();
  
  timestamp = Math.round((curr_time - getCookieValue("start_time") )/ 1000)
  blender_level = document.getElementById("Blender").value  
  // wattage = inductionPowerMap[induction_level]

  document.getElementById("Logs").value += timestamp 
  + ", Blender level: " + blender_level +"\n"


  // Update UI
  $("#IL_Text").html(blender_level);
});

var num_loop = 0

$( "#add_loop" ).click(function(e) {
  
  $( "#loop" ).append(
    '<div class="form-row"><div class="form-group col"><label for="exampleFormControlInput1">Level ' +num_loop + ':</label><input type="number" class="form-control" min="0" max="3" id="Level_' +num_loop + '"></div><div class="form-group col"><label for="exampleFormControlInput1">Time ' +num_loop + ':</label><input type="number" class="form-control" min="0" max="3" id="Time_' +num_loop + '"></div></div><br>'
  )
  num_loop +=1
  e.preventDefault();  
});

$( "#start_loop" ).click(function(e) {
  form_data = {}

  var d = new Date();
  var curr_time = d.getTime();
  
  timestamp = Math.round((curr_time - getCookieValue("start_time") )/ 1000)
  blender_level = document.getElementById("Blender").value  
  total_time = document.getElementById("total_time").value  

  document.getElementById("Logs").value += timestamp 
  + ", loop set, total_time: " + total_time


  var end_string = ", levels: "
  for (let i = 0; i < num_loop; i++) {
    form_data['level_'+i] = document.getElementById("Level_"+ i).value
    form_data['time_'+i] = document.getElementById("Time_"+ i).value
    end_string += form_data['level_'+i] + " for " + form_data['time_'+i] + " seconds, "
  }
  end_string = end_string.substring(0, end_string.length - 2);
  // end_string -=", "
  document.getElementById("Logs").value += end_string + "\n"


  e.preventDefault();  
});