from django.db import models

class InductionTypeChoices(models.TextChoices):
    INDUCTION0 = '0', '0'
    INDUCTION1 = '300', '1'
    INDUCTION2 = '500', '2'
    INDUCTION3 = '800', '3'
    INDUCTION4 = '1000', '4'
    INDUCTION5 = '1200', '5'
    INDUCTION6 = '1600', '6'
    INDUCTION7 = '2000', '7'
    INDUCTION8 = '2500', '8'