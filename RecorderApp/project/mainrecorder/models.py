from django.db import models

from mainrecorder.choices import (
    InductionTypeChoices)

class LogFile(models.Model):
    uid = models.IntegerField()
    log = models.TextField()

class State(models.Model):
    uid = models.TextField()
    induction_level = models.TextField(default="0")
    blender_level = models.TextField(default="0")
    hardware_error = models.BooleanField(default=False)
    induction_level = models.TextField(default="0")
    weight = models.TextField(default="0")
    temp = models.TextField(default="0")