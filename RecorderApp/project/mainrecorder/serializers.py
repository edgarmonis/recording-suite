from django.contrib.auth.models import User, Group
from rest_framework import serializers

from mainrecorder.models import LogFile, State#, TempFile, WeightFile, BlenderLevelFile, HardwareErrorFile) Induction,


class LogSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LogFile
        fields = ['uid','log']

class StateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = State
        fields = [
                'uid','induction_level','blender_level','hardware_error',
                'induction_level', 'weight', 'temp'
            ]