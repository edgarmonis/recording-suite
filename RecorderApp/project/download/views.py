import os, tempfile, zipfile, json, requests, csv, urllib
import subprocess, pexpect

from django.http import HttpResponse, FileResponse, JsonResponse
from wsgiref.util import FileWrapper
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from django.shortcuts import render
from django.core import serializers

from mainrecorder.views import CLIENT
from mainrecorder.models import LogFile#Induction, 
from utils.log_and_map import logger, fpsIPMap


def create_log_file(request, uid, download_path):
    if request.COOKIES['record_type']=="IN":
        equipment = request.COOKIES['fps_equipment'].lower()
    elif request.COOKIES['record_type']=="BL":
        equipment = request.COOKIES['blender_equipment'].lower()

    log = LogFile.objects.filter(uid=uid)

    qs_json = json.loads(serializers.serialize('json', log))
    log_text = qs_json[0]['fields']['log']

    csv_path = os.path.join(download_path,'recordingLogs.csv')
    f = open(csv_path, "a")
    f.write(log_text)
    f.close()

def send_log_file_to_mongo(request, uid):
    recipe_name = request.COOKIES['sub_recipe']
    quantity = request.COOKIES['quantity']
    version = request.COOKIES['version']


    log = LogFile.objects.filter(uid=uid)
    qs_json = json.loads(serializers.serialize('json', log))
    log_text = qs_json[0]['fields']['log']

    data = {
        "recipe": recipe_name,
        "quantity": quantity,
        "version": version,
        "logs": log_text,
    }

    r = requests.get(url = os.getenv("MONGO_LOG_URL"), params = data)

def get_rpi_data(equipment, title, time_started, DOWNLOAD_FILE_PATH):    
    big_command = "pi@" + fpsIPMap[equipment] + ":/home/pi/Desktop/RecordingData/" + title + "_" + str(int(time_started)) + "/" 
    path = DOWNLOAD_FILE_PATH+"/."
    
    new_command = "rsync -avz pi@" + fpsIPMap[equipment] + ":/home/pi/Desktop/RecordingData/" + title + "_" + str(int(time_started)) + "/ "+DOWNLOAD_FILE_PATH+"/."
    events={ 'assword:': "CloudChef12#\r", '\? ': "yes\r"}
    new_run = pexpect.run(new_command, events=events)
    
    logger.info(new_command)
    logger.info("data copied")

def get_download(request):
    uid = request.COOKIES['uid']
    title = urllib.parse.unquote(request.COOKIES['record_name'])
    quantity = urllib.parse.unquote(request.COOKIES['quantity'])
    version = urllib.parse.unquote(request.COOKIES['version']).replace('/','-')

    time_started = request.COOKIES['time_started']
    
    # Get equipment
    equipment = request.COOKIES['equipment'].lower()

    temp = './zip_file/'+ title + "_" + str(quantity) + "_" + version +'.zip'
    temp = temp.replace(' ','_')
    archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)
    DOWNLOAD_FILE_PATH = './download_file' + "/" + title + "_" + str(quantity) + "_" + version
    DOWNLOAD_FILE_PATH = DOWNLOAD_FILE_PATH.replace(' ','_')

    try:
        os.mkdir(DOWNLOAD_FILE_PATH)
    except:
        print("dir exists")

    send_log_file_to_mongo(request, uid)
    create_csv_file(request, uid, DOWNLOAD_FILE_PATH)
    create_log_file(request, uid, DOWNLOAD_FILE_PATH)
    
    get_rpi_data(equipment,title, time_started, DOWNLOAD_FILE_PATH)    
    CLIENT.unsubscribe(equipment+"/Rec1")

    for _file in os.listdir(DOWNLOAD_FILE_PATH):
        filename = os.path.join(DOWNLOAD_FILE_PATH, _file)
        archive.write(
            filename, 
            os.path.join( title + "_" + str(int(time_started) ) , _file ) 
        )
    archive.close()

    zip_file = open(temp, 'rb')
    return FileResponse(zip_file, filename=title + "_" + str(int(time_started) )+'.zip')

def get_zip(request):
    title = urllib.parse.unquote(request.COOKIES['record_name'])
    quantity = urllib.parse.unquote(request.COOKIES['quantity'])
    version = urllib.parse.unquote(request.COOKIES['version']).replace('/','-')

    temp = './zip_file/'+ title + "_" + str(quantity) + "_" + version +'.zip'
    temp = temp.replace(' ','_')

    zip_file = open(temp, 'rb')
    return FileResponse(zip_file, filename=title + "_" + str(quantity) + "_" + version +'.zip')

def create_csv_file(request, uid, download_path):
    if request.COOKIES['record_type']=="IN":
        equipment = request.COOKIES['fps_equipment'].lower()
    elif request.COOKIES['record_type']=="BL":
        equipment = request.COOKIES['blender_equipment'].lower()

    log = LogFile.objects.filter(uid=uid)

    qs_json = json.loads(serializers.serialize('json', log))
    log_text = qs_json[0]['fields']['log']

    rows = []
    rows = log_text.split('\n')

    header = ["Temperature", "Weight reading", "Timestamp", "Step type", "Instruction", "Vessel type(add)", "Ingredient name", "Quantity",
        "Unit", "Graphic", "source", "destination", "Power(induction)", "Extra1", "Extra2"]

    data_list = []
    acc_list = []
    recording_timestamp = None
    record_timestamp = None
    for pure_row in rows:
        row = pure_row.split(',') 
        
        if len(row) > 1:
            time = row[0]
            command = row[1].strip()
            print(command)
            data = []

            if record_timestamp:
                recording_timestamp = time
                record_timestamp = False

            if command.startswith("Add:"):
                data = ["","",time,"add", "", "", command.replace("Add:","").strip(), "", "g", "", "", "", "", "", ""]

            elif command.startswith("Action:"):
                data = ["","",time,"ha", command.replace("Action:","").strip(), "", "", "", "", "", "", "", "", "", ""]

            elif command.startswith("Stir: Started"):
                data = ["","",time,"stir_start", "", "", "", "", "", "", "", "", "", "", ""]

            elif command.startswith("Stir: Stopped"):
                data = ["","",time,"stir_stop", "", "", "", "", "", "", "", "", "", "", ""]
            
            elif command.startswith("Capture:") or command.startswith("Tare:"):
                data = ["","",time,"comment", command.strip(), "", "", "", "", "", "", "", "", "", ""]

            # CONFIRM
            elif command.startswith("Accesories:"):
                data = ["","",time,"comment", command.replace("Accesories:","").strip(), "", "", "", "", "", "", "", "", "", ""]
                acc_list.append(command.replace("Accesories:","").strip())

            elif command.startswith("Recording:"):
                data = ["","",time,"comment", command.strip(), "", "", "", "", "", "", "", "", "", ""]
                
                if record_timestamp is None:
                    record_timestamp = True

            elif command.startswith("Comment:"):
                data = ["","",time,"comment", command.replace("Comment:","").replace("Recording:","").strip() , "", "", "", "", "", "", "", "", "", ""]

            elif command.startswith("Induction level: "):
                ind_level = command.replace("Induction level: ","").strip()
                ind_level = ind_level.split(' ')[1].strip('(').strip(')')
                data = ["","",time,"induction", "" , "", "", "", "", "", "", "", ind_level]
            
            if len(data)>0:
                data_list.append(data)
        else:
            data = ["","","","comment", pure_row.strip() , "", "", "", "", "", "", "", "", "", ""]
            if len(data)>0:
                data_list.append(data)
    
    acc_command = "Fetch "
    for acc in acc_list:
        acc_command += acc + " AND "        
    acc_command = acc_command.strip('AND ')

    accessored_command_list = ["","",recording_timestamp,"ha", acc_command, "", "", "", "", "", "", "", "", "", ""]

    acc_list = [         
        ["Equipment Type","","","", "", "", "", "", "", "", "", "", "", "", ""],
        ["Vessels","","","", "", "", "", "", "", "", "", "", "", "", ""],
        ["Mapping","","","", "", "", "", "", "", "", "", "", "", "", ""],
        header
    ]
    data_list.insert(1,accessored_command_list)

    data_list = acc_list + data_list

    csv_path = os.path.join(download_path,'RecipeData.csv')
    new_csvfile = open(csv_path, 'w')

    wr = csv.writer(new_csvfile, quoting=csv.QUOTE_ALL)
    for data in data_list:
        wr.writerow(data)    

    new_csvfile.close()