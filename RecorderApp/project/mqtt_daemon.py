
import json
import os
import pickle
import requests
import threading
import subprocess
import time

from multiprocessing.connection import Listener
import paho.mqtt.client as paho

from utils.mqtt import NetworkMessage

address = ('localhost', 6000)
listener = Listener(address, authkey=b"ch6gtfg12#")
conn = listener.accept()

def listener_on_message(client=None, userdata=None, message=None):
    recvMsg = json.loads(message.payload.decode("utf-8"))
    # recvMsg = {'function': 'temperatureRead', 'payload': 30, 'receiverId': 'Rec1', 'senderId': 'fps4', 'taskId': None}
    print("listener recieved mqtt from" + recvMsg['senderId'])
    msg_string = json.dumps(recvMsg)
    resp = requests.get("http://0.0.0.0:8000/main/on-mqtt-message", params={"msg":msg_string})

# broker = "192.168.0.112"
broker = os.getenv("BROKER")
client = paho.Client()
client.on_message = listener_on_message
client.connect(broker, 1883, keepalive=60)
client.loop_start()

while True:
    msg = conn.recv()
    # do something with msg
    print(msg)
    if msg == 'close':
        conn.close()
        break
    elif msg[0] == 'sub':
        client.subscribe(msg[1])
    elif msg[0] == 'unsub':
        client.unsubscribe(msg[1])
    elif msg[0] == 'pub':
        equipment = msg[1].lower()
        function = msg[2]
        payload = msg[3]
        nMessage = NetworkMessage(receiverId = equipment, function = function)
        
        # Parse numbers for numeric payloads
        if function == "setLevel" or function == "inductionLevel":
            payload = int(payload)

        # Assign payload
        nMessage.payload = payload
        rc = client.publish(nMessage.getChannel(), nMessage.getMessage(), qos=2)
        print("MQTT Message published to "+ nMessage.getChannel())

    conn = listener.accept()

listener.close()
# {'function': 'temperatureRead', 'payload': 30, 'receiverId': 'Rec1', 'senderId': 'fps4', 'taskId': None}