#!/bin/bash
sleep $DB_INIT_SLEEP
cd /app/recorder/project
python manage.py makemigrations frontpage_apis mainrecorder
python manage.py migrate frontpage_apis
python manage.py migrate mainrecorder

# ssh -o StrictHostKeyChecking=no pi@192.168.0.55
# ssh -o StrictHostKeyChecking=no pi@192.168.0.79
# ssh -o StrictHostKeyChecking=no pi@192.168.0.70
# ssh -o StrictHostKeyChecking=no pi@192.168.0.82
# ssh -o StrictHostKeyChecking=no pi@192.168.0.80

# python manage.py runserver 0.0.0.0:8000
python3 -u mqtt_daemon.py > mqtt.log &
newrelic-admin run-program gunicorn --bind=0.0.0.0:8000 -c /app/recorder/project/docker_gunicorn_configuration.py project.wsgi:application --preload
# gunicorn --bind=0.0.0.0:8000 --workers $NUM_WORKERS -c /app/recorder/project/docker_gunicorn_configuration.py project.wsgi:application --preload