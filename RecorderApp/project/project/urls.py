"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from frontpage_apis import views as RecordViews
from mainrecorder import views as MainRecorderViews



from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# ... the rest of your URLconf goes here ...
router = routers.DefaultRouter()
router.register(r'records', RecordViews.RecordsViewSet)
# router.register(r'induction', MainRecorderViews.InductionViewSet)
router.register(r'logs', MainRecorderViews.LogViewSet)
router.register(r'state', MainRecorderViews.StateViewSet)
# router.register(r'temp', MainRecorderViews.TempViewSet)
# router.register(r'weight', MainRecorderViews.WeightViewSet)
# router.register(r'bl-level', MainRecorderViews.BlenderLevelViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('home/', include('frontpage_apis.urls')),
    path('start/', RecordViews.get_selector, name='get_selector'),
    path('main/', include('mainrecorder.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += staticfiles_urlpatterns()
