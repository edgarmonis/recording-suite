
# Recorder APP

## The recorder app performs 4 main functions

1) [Send](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/utils/mqtt.py#L75) MQTT messages 
2) [Recieve](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/utils/recieve_message.py) and display MQTT messages
3) [Create](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/download/views.py#L17) a log file
4) [Create](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/download/views.py#L44) a zip file of all the contents


## Design decisons

1) UI updates are handled via short polling

    The relevant polling APIS are:
    - http://0.0.0.0:8000/main/get-temp         [code](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/views.py#L97)
    - http://0.0.0.0:8000/main/get-weight       [code](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/views.py#L107)
    - http://0.0.0.0:8000/main/get-error        [code](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/views.py#L118)
    - http://0.0.0.0:8000/main/get-bl-level     [code](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/views.py#L132)


    Because these values are accessed often, they are stored in memory via a
    dictionary [at](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/utils/message_handler.py)
    When a message arrives from MQTT, the above dictionary is updated.
    The code logic is placed [in](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/utils/message_handler.py)

2) Form validation and showing / not showing of elements is handled via js

    Relevant files :
    - [Frontpage](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/frontpage_apis/templates/index.html)
    - [Main Recorder UI](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/templates/base_generic_main.html)
    - [Induction UI](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/templates/induction.html)
    - [Controlled Blended UI](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/templates/blender_controlled.html)
    - [Manual Blended UI](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/templates/blender_manual.html) Note : This is empty becuase there is no custom logic

3) When an equipement button is pressed / a control change occurs,
    an MQTT message is [sent](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/utils/mqtt.py#L75) and the logs are updated in the frontend itself


4) After the frontpage is passed, an init [function](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/mainrecorder/views.py#L69) is run

5) When End Recording is pressed, a download [function](https://gitlab.com/edgarmonis/recording-webapp/-/blob/main/project/download/views.py#L44) is run
